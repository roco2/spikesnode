export class Index {

  calcularAreaCuadrado(lado: number) {
    let response;

    if (lado === 0) {
      response = "Error el resultado debe ser mayor a 0";
    }
    response = lado * lado;
    return response;
  }

  calcularAreaRectangulo(base: number, altura: number) {
    return base * altura;
  }

  calcularAreaTriangulo(base: number, altura: number) {
    return (base * altura) / 2;
  }

  methodUncovered(base: number, altura: number) {
    return (base * altura) / 2;
  }
}
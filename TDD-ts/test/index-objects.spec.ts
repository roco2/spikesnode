import { User } from './../src/index-objects';


describe('Comparadores', () => {
    const user = new User();
    const user1 = {
        name: "Roco",
        lastame: "Rojas2"
    };

    it('No son iguales', () =>{
        expect(user1).not.toEqual(user.mapperUser());
    });
});
import { Index } from './../src/index';


describe('caluculador de areas', () => {
    const index = new Index();
    it('calucular el area de un cuadrado de 5 x 5', () => {
        const resultado = index.calcularAreaCuadrado(5);
        expect(resultado).toBe(25);
    });

    it('Calcular area de un cuadrado  de 2 x 2', () => {
        const resultado = index.calcularAreaCuadrado(2);
        expect(resultado).toBe(4);
    });

    it('El valor ingresado no puede ser 0', () => {
        const resultado = index.calcularAreaCuadrado(0);
        expect(resultado).toEqual(0);
    });

    it('Calcular area de un rectangulo  de 2 x 4', () => {
        const resultado = index.calcularAreaRectangulo(2, 4);
        expect(resultado).toBe(8);
    });

    it('Calcular area de un triangulo  de 12 x 20', () => {
        const resultado = index.calcularAreaTriangulo(12, 20);
        expect(resultado).toBe(120);
    });
});
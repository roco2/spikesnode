using System.Text;
using tusdotnet;
using tusdotnet.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
//builder.WebHost.ConfigureKestrel((ctx, opt) =>
//{
//    opt.Limits.MaxRequestBodySize = 2684354560;
//});

var app = builder.Build();

app.MapTus("/test", async httpContext => new()
{
    // This method is called on each request so different configurations can be returned per user, domain, path etc.
    // Return null to disable tusdotnet for the current request.

    // Where to store data?
    Store = new tusdotnet.Stores.TusDiskStore(@"C:\SO\colorinfiles\"),
    Events = new()
    {
        OnFileCompleteAsync = async eventContext =>
        {
            ITusFile file = await eventContext.GetFileAsync();
            if (file != null)
            {
                var fileStream = await file.GetContentAsync(httpContext.RequestAborted);
                var metadata = await file.GetMetadataAsync(httpContext.RequestAborted);

                httpContext.Response.ContentType = metadata.ContainsKey("contentType")
                                                 ? metadata["contentType"].GetString(Encoding.UTF8)
                                                 : "application/octet-stream";

                //Providing New File name with extension
                string name = "NewFileName.jpg";
                string networkPath = @"C:\tusfiles\";

                using (var fileStream2 = new FileStream(networkPath + "\\" + name, FileMode.Create, FileAccess.Write))
                {
                    await fileStream.CopyToAsync(fileStream2);
                }
            }
        }
    }
});

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net.Http.Headers;

[ApiController]
[Route("[controller]")]
public class FileController : ControllerBase
{
    private readonly ILogger<FileController> _logger;
    public static IHostEnvironment? _environment;
    public FileController(ILogger<FileController> logger, IHostEnvironment? environment)
    {
        _logger = logger;
        _environment = environment;
    }

    [HttpPost(Name = "FileUp")]
    [DisableRequestSizeLimit]
    public async Task<IActionResult> FileUp(IFormFile file, string flag)
    {
        // Check if the request contains multipart/form-data.
        if (file.FileName == null)
        {
            return new UnsupportedMediaTypeResult();
        }

        if (file.Length > 0)
        {
            IFormFile formFile = file;

            var folderPath = Path.Combine(_environment.ContentRootPath, "statics");

            var filePath = Path.Combine(folderPath, formFile.FileName);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            var buffer = 2048 * 2048;
            using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None, buffer, useAsync: false))
            {
                await formFile.CopyToAsync(fileStream);
                await fileStream.FlushAsync();
                return Ok(new { status = "Upload Success", length = formFile.Length, name = formFile.FileName });
            }
        }
        else
        {
            return NotFound("Failed to upload");
        }
    }


    //public async Task<Boolean> UploadFileAsync(Guid id, string name, Stream file)
    //{
    //    int chunckSize = 2097152; //2MB
    //    int totalChunks = (int)(file.Length / chunckSize);
    //    if (file.Length % chunckSize != 0)
    //    {
    //        totalChunks++;
    //    }

    //    for (int i = 0; i < totalChunks; i++)
    //    {
    //        long position = (i * (long)chunckSize);
    //        int toRead = (int)Math.Min(file.Length - position, chunckSize);
    //        byte[] buffer = new byte[toRead];
    //        await file.ReadAsync(buffer, 0, buffer.Length);

    //        using (MultipartFormDataContent form = new MultipartFormDataContent())
    //        {
    //            form.Add(new ByteArrayContent(buffer), "files", name);
    //            form.Add(new StringContent(id.ToString()), "id");
    //            var meta = JsonConvert.SerializeObject(new ChunkMetaData
    //            {
    //                UploadUid = id.ToString(),
    //                FileName = name,
    //                ChunkIndex = i,
    //                TotalChunks = totalChunks,
    //                TotalFileSize = file.Length,
    //                ContentType = "application/unknown"
    //            });
    //            form.Add(new StringContent(meta), "metaData");
    //            var response = await Client.PostAsync("/api/Upload", form).ConfigureAwait(false);
    //            return response.IsSuccessStatusCode;
    //        }
    //    }
    //    return true;
    //}

}


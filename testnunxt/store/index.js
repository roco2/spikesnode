
// Este será el alamacenamiento central
export const state = () => ({
  _counter: 0,
});


// Accesores o getters (State)
// Normalmente se llaman des de las coputed en los componentes

export const getters = {
  getCounter(state){
    return state._counter;
  }
}


//Mutadores (Mutation)
// Su finalidad es modificar el state o almacenamiento

export const mutations = {
  upCounter(state){
    return state._counter++;
  }
}


// Acciones (Action)
// Estas funciones sirven para modificar las mutaciones
// A diferencia de las mutaciones pueden ser asincronas
// Pueden contener logica de negocio
// Pueden llamar a varias mutaciones

export const actions = {
  // sumarDos(vuexContext){
  //   vuexContext.commit('upCounter');
  // }

  /**
   * Esta funcion se ejecuta al inicializarse la APP
   * @param {*} vuexContext commit, dispatch, state
   * @param {*} context es el mismo de asyncData y aqui tenemos acceso a toda la app, $axios, env, store etc
   */
  async nuxtServerInit(vuexContext, context){
    await vuexContext.dispatch('posts/loadPosts');
  },
  sumarDos({commit}){
    commit('upCounter');
    commit('upCounter');
  }
}





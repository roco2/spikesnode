export const state = () => ({
  _posts: []
});


//Actions
export const actions = {
  async loadPosts({commit}){
    let posts = await this.$axios.get('https://jsonplaceholder.typicode.com/posts');
    //Realizar commit
    commit('setPosts', posts.data);
  }
}

// Getters
export const getters = {
  getPosts(state){
    return state._posts;
  }
}

// Mutaciones
export const mutations = {
  setPosts(state, posts){
    state._posts = posts;
  }
}

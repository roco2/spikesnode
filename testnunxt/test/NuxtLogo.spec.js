import { mount } from '@vue/test-utils';
import NuxtLogo from '@/components/NuxtLogo.vue';
import Tutorial from '@/components/Tutorial.vue';

describe('NuxtLogo', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(NuxtLogo);
    expect(wrapper.vm).toBeTruthy()
  });
});

describe('Tutorial', () => {
  test('Count props', () => {
    const component = mount(Tutorial);
    expect(component.props.length).toEqual(1);
  });
});

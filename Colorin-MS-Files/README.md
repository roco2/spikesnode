# Colorin-MS-Files #

Contiene código fuente que se encarga de realizar la carga de archivos que pesen más de 2Gigas al blob-storage. 
Entre otras funciones también convierte las imágenes que estén formato tiff a un jpg más liviano, este proceso se relaciona con la carga de imágenes para utilizadas en las artes.

## Requisitos previos

- .NET 6.0

## Descripción

Este proyecto backend en C# es una aplicación que proporciona servicios y funcionalidades de API. Está desarrollado utilizando .NET y sigue las mejores prácticas de arquitectura y seguridad.

## Estructura del proyecto

El proyecto está organizado de la siguiente manera:

Colorin-MS-Files/
├── Api/
│ ├── [Proyecto de la API]
├── Application/
├── .gitignore
├── [Otros archivos y directorios]

Explicación de los archivos y directorios:

- `README.md`: Este archivo que proporciona información sobre el proyecto.
- `.gitignore`: Archivo que especifica los archivos o directorios que deben ser ignorados por el control de versiones.

### Dependencias ###

- Azure.Storage.Blobs
- Colorin.Transversal.Tracking
- Magick.NET-Q16-AnyCPU
- Microsoft.ApplicationInsights
- Microsoft.AspNetCore.Authentication.JwtBearer
- Microsoft.AspNetCore.Http.Features
- Microsoft.AspNetCore.ResponseCompression
- Microsoft.Identity.Web
- SixLabors.ImageSharp
- Swashbuckle.AspNetCore

1. Clonar este repositorio en tu máquina local desde: https://swocolorin@dev.azure.com/swocolorin/Application%20Modernization/_git/Colorin-MS-Files
2. Abrir el explorador y navegar al directorio raíz del proyecto.
3. Ubicar y abrir el archivo con extension ".sln".

4. Seleccionar el proyecto: "Api", dar click derecho y elegir la opcion: "Manage Nuget Packages"

## Configuración del entorno

Para configurar el entorno de desarrollo local, seguir estos pasos:
1. Instala .NET SDK. Puedes descargarlo desde el sitio oficial de Microsoft.

2. Seleccionar el proyecto: "Api", dar click derecho y elegir la opcion: "Manage Nuget Packages"
### Adicionar Nuget de Colorin ###

| Paso # | Accion  |
| ------------ | ------------ |
|1| Seleccionar el proyecto "*Api*"|
|1.1| Dar click derecho y elegir la opcion: **Manage Nuget Packages** |
|2|  Click izquierdo en **icono Engranaje** |
|3|  Click izquierdo en **botón (más verde) +** para adicionar un package Source  |
|3.1| Llenar **nombre** con "Colorin" |
|3.2| Llenar **source** con "https://pkgs.dev.azure.com/swocolorin/152ba74d-af41-4b7e-87d5-29c318ecb231/_packaging/ColorinFeed/nuget/v3/index.json"  |
|4| Dar click izquierdo en en **botón Ok** para Finalizar |
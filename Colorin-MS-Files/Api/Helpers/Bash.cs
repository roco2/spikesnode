﻿using System.Diagnostics;

namespace Api.Helpers
{
    public class Bash
    {
        public async Task<bool> ExecuteCommand(string command)
        {
            string output = "";
            string error = "";
            bool response = false;
            using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
            {
                proc.StartInfo.FileName = "/bin/bash";
                proc.StartInfo.Arguments = "-c \"" + command + "\"";
                //proc.StartInfo.FileName = "wsl"; // cambiar para pruebas locales
                //proc.StartInfo.Arguments = command;

                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.Start();

                await proc.WaitForExitAsync();

                output = await proc.StandardOutput.ReadToEndAsync();
                error = await proc.StandardError.ReadToEndAsync();

                if (proc.ExitCode != 0)
                {
                    Logs.CreateLog(error);
                    throw new Exception(error);
                }
            }

            response = true;

            if (!string.IsNullOrEmpty(error) && (error.Contains("ERROR") || error.Contains("error")))
            {
                Logs.CreateLog(error);
                response = false;
                throw new Exception(error);
            }

            return response;
        }

        public async Task<object> ExecuteCommand2(string command)
        {
            string output = "";
            string error = "";
   
            using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
            {
                proc.StartInfo.FileName = "/bin/bash";
                proc.StartInfo.Arguments = "-c \"" + command + "\"";
                //proc.StartInfo.FileName = "wsl"; // cambiar para pruebas locales
                //proc.StartInfo.Arguments = command;

                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.Start();


                await proc.WaitForExitAsync();

                output = await proc.StandardOutput.ReadToEndAsync();
                error = await proc.StandardError.ReadToEndAsync();

                if (proc.ExitCode != 0)
                {
                    Logs.CreateLog(error);
                    throw new Exception(error);
                }
            }

            var response = new { command, output, error };

            if (!string.IsNullOrEmpty(error) && (error.Contains("ERROR") || error.Contains("error")))
            {
                Logs.CreateLog(error);
                throw new Exception(error);
            }

            return response;
        }
    }
}

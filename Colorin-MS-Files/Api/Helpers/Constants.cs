﻿namespace Api.Helpers
{
    public struct Configurations
    {
        public const string Folder = "Folder";
        public const string Memory = "memory-limit";
        public const string Disk = "disk-limit";
        public const string RAMSZ = "Ram";
        public const string HDSZ = "Hd";
        public const string extension = "extension";
        public const string accountName = "accountName";
        public const string containerName = "containerName";
        public const string accountKey = "accountKey";
        public const string maxsize = "maxsize";
        public const string azBash = "curl -sL https://aka.ms/InstallAzureCLIDeb | bash";

    }
    public struct Routes
    {
        public const string Upload = "upload";
        public const string Merge = "merge";
        public const string UploadBlob = "uploadblob";
        public const string Upload2 = "upload2";
        public const string Delete = "delete";
    }

    public struct Messages
    {
        public const string NoFile = "No file was selected.";
        public const string FileExist = "A file with the same name already exists.";
        public const string UploasSuccess = "File uploaded successfully.";
        public const string NoExist = "The specified file does not exist.";
        public const string NoExistChunks = "No file chunks were found.";
        public const string Base64Error = "Erro to converte the image Base64: ";
        public const string FileChunckMerge = "File chunks merged successfully";
        public const string FileWasNotConverted = "The image was not converted or is empty the base64";
        public const string ErroModelBlob = "The model is empty";
        public const string ErroModelRoutesBlob = "The structure for the model Filename is not correct";
    }

    public struct Commands
    {
        public const string DeleteCommand = "cd Uploads; rm -rf ";
    }
}

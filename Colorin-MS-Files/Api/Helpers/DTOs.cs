﻿namespace Api.Helpers
{
    public class DTOs
    {
    }

    public class FileStorage
    {
        public string? FileName { get; set; }
        public string? Routes { get; set; }
    }

    public class ResponseImage
    {
        public long? size { get; set; }
        public string? message { get; set; }
        public string? img { get; set; }
    }
}

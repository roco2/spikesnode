﻿namespace Api.Helpers
{
    public static class Logs
    {
        public static void CreateLog(string message, bool error = false)
        {
            if (!Directory.Exists("Logs"))
            {
                Directory.CreateDirectory("Logs");
            }

            var logFilePath = Path.Combine("Logs", "logError.txt");

            string timeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string formattedMessage = error ? $"[{timeStamp}] - {message}{Environment.NewLine}" : $"[ERROR] - {message}{Environment.NewLine}";

            File.AppendAllText(logFilePath, formattedMessage);
        }
    }
}

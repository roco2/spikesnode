﻿namespace Api.Helpers
{
    using Colorin.Transversal.Tracking.Common;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage] 
    public class ProjectInformation : IProjectInformation 
    { private const string ProjectName = "Colorin-MS-Files"; public string GetNameProject() => ProjectName; }
}

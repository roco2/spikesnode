﻿namespace Api.Controllers
{
    using Api.Helpers;
    using Azure;
    using ImageMagick;
    using Microsoft.AspNetCore.Mvc;
    using System.Buffers.Text;
    using System.IO.Compression;
    using System.IO;
    using System.Net.Http.Headers;
    using System.Text.RegularExpressions;
    using Microsoft.AspNetCore.Authorization;

    [Authorize]
    public class FileController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private static string folder;
        private readonly string _targetFolder;
        public FileController(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
            _targetFolder = Path.Combine(Environment.CurrentDirectory, _configuration.GetValue<string>(Configurations.Folder));
            _webHostEnvironment = webHostEnvironment;
        }


        [HttpPost(Routes.Upload)]
        [RequestSizeLimit(8589934592)]
        public async Task<IActionResult> UploadAsync(IFormFile file)
        {

            //var files = Request.Form.Files;

            if (file == null || file.Length == 0)
            {
                return BadRequest(Messages.NoFile);
            }
            if (!Directory.Exists(_targetFolder))
            {
                Directory.CreateDirectory(_targetFolder);
            }
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName?.Trim('"');
            var filePath = Path.Combine(_targetFolder, fileName);

            if (System.IO.File.Exists(filePath))
            {
                return Conflict(Messages.FileExist);
            }
            // Create a new file
            await Task.Run(() => ProcessFile(filePath, file));

            return Ok(Messages.UploasSuccess);
        }

        private async Task ProcessFile(string filePath, IFormFile file)
        {
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
        }

        [HttpGet(Routes.Merge)]
        public async Task<IActionResult> Merge(string fileName)
        {

            var filePath = Directory.GetFiles(_targetFolder, $"{fileName}-*")[0];

            if (!System.IO.File.Exists(filePath))
            {
                return BadRequest(Messages.NoExist);
            }

            var chunkFiles = Directory.GetFiles(_targetFolder, $"{Path.GetFileName(fileName)}-*");

            if (chunkFiles.Length == 0)
            {
                return BadRequest(Messages.NoExistChunks);
            }

            var sortedFiles = chunkFiles.OrderBy(f => GetFileNumber(f)).ToList();

            using (var outputStream = new FileStream(Path.Combine(_targetFolder, fileName), FileMode.Create))
            {
                foreach (var chunkFile in sortedFiles)
                {
                    using (var inputStream = new FileStream(chunkFile, FileMode.Open))
                    {
                        await inputStream.CopyToAsync(outputStream);
                    }
                    System.IO.File.Delete(chunkFile);
                }
            }

            var responseImageFormat = await changeFormatImage(fileName);

            if (responseImageFormat != null)
            {
                return Ok(responseImageFormat);
            }
            else
            {
                return BadRequest(new { message = Messages.FileWasNotConverted, img = string.Empty });
            }
        }

        [HttpPost(Routes.UploadBlob)]
        public async Task<IActionResult> UploadBlob([FromBody] FileStorage fileStorage)
        {
            var extension = _configuration.GetValue<string>(Configurations.extension);
            var filenameJpeg = $"{fileStorage.FileName.Split('.')[0]}.{extension}";

            if ((fileStorage == null) || (string.IsNullOrEmpty(fileStorage.FileName)) || (string.IsNullOrEmpty(fileStorage.Routes)))
            {
                return BadRequest(Messages.ErroModelBlob);
            }

            var folder = _configuration.GetValue<string>(Configurations.Folder);
            var accountName = _configuration.GetValue<string>(Configurations.accountName);
            var containerName = _configuration.GetValue<string>(Configurations.containerName);
            var accountKey = _configuration.GetValue<string>(Configurations.accountKey);

            await new Bash().ExecuteCommand($"cd {folder}; az storage blob upload --type block --file {fileStorage.FileName} --account-name {accountName} --container-name {containerName}/{fileStorage.Routes} --name {fileStorage.FileName} --account-key {accountKey}");
            await new Bash().ExecuteCommand($"cd {folder}; az storage blob upload --type block --file {filenameJpeg} --account-name {accountName} --container-name {containerName}/{fileStorage.Routes} --name {filenameJpeg} --account-key {accountKey}");
            return Ok(true);
        }

        public async Task<string> ImageToBase64(string fileName)
        {
            
            string base64String = string.Empty;
            try
            {
                var filePath = Directory.GetFiles(_targetFolder, $"{fileName}")[0];
                byte[] imageBytes = await System.IO.File.ReadAllBytesAsync(filePath);
                base64String = Convert.ToBase64String(imageBytes);

            }
            catch (Exception ex)
            {
                Logs.CreateLog($"ImageToBase64, {ex.Message}");
                throw new Exception(Messages.Base64Error + ex.Message);
            }

            return base64String;
        }
        private static int GetFileNumber(string fileName)
        {
            string numberString = new string(fileName.Where(char.IsDigit).ToArray());
            if (int.TryParse(numberString, out int number))
            {
                return number;
            }
            return 0;
        }
        private async Task<ResponseImage> changeFormatImage(string fileName)
        {
            var filePath = Path.Combine(_targetFolder, fileName);
            var extension = _configuration.GetValue<string>(Configurations.extension);
            var pathConvert = Path.Combine(_targetFolder, $"{fileName.Split('.')[0]}.{extension}");
            var toBase64 = string.Empty;

            using (var image = new MagickImage())
            {
         
                image.Settings.SetDefine(MagickFormat.Jpeg, Configurations.Memory, _configuration.GetValue<string>(Configurations.RAMSZ));
                image.Settings.SetDefine(MagickFormat.Jpeg, Configurations.Disk, _configuration.GetValue<string>(Configurations.HDSZ));
                image.Read(filePath, MagickFormat.Tif);
                image.Quality = 50;
                image.Strip();
                image.Format = MagickFormat.Jpeg;
                image.Scale(percentage: (Percentage)5.0);
                await image.WriteAsync(pathConvert);
                toBase64 = await ImageToBase64($"{fileName.Split('.')[0]}.{extension}");
            }

            var size = new System.IO.FileInfo(pathConvert).Length;
            var response = new ResponseImage() { message = Messages.UploasSuccess, img = toBase64, size = size };

            return response;

        }


        [HttpPost(Routes.Delete)]
        public async Task<IActionResult> Delete(string filename)
        {

            var response = await new Bash().ExecuteCommand($"{Commands.DeleteCommand}{filename}");
            return Ok(response);
        }

        #region For Support
        //[HttpPost("bash")]
        //public async Task<IActionResult> cmdBash(string cmd)
        //{

        //    var response = await new Bash().ExecuteCommand2($"{cmd}");
        //    return Ok(response);
        //}
        #endregion

    }

}







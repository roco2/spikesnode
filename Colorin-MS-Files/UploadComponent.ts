uploadBigImage(fileToUpload: File) {
    console.log('Imagen es grande...')
    if (!fileToUpload) {
      console.log('Selecciona un archivo primero');
      return;
    }
    this.uploading = true;
    this.blocked = true;
    const chunkSize = parseInt(environment.MaxImageSizeSuppoted.toString()) / 5;
    const fileSize = fileToUpload.size;
    const chunks = Math.ceil(fileSize / chunkSize);

    // console.log('Comenzando subida de archivo...');
    // console.log('Tamaño total del archivo: ' + fileSize + ' bytes');
    // console.log('Tamaño de cada chunk: ' + chunkSize + ' bytes');
    // console.log('Número total de chunks: ' + chunks);
    let currentChunk = 0;
    const uploadFileChunk = (file: File, start: number, end: number) => {
      console.log('Subiendo chunk ' + (currentChunk + 1) + ' de ' + chunks);
      const formData = new FormData();
      const chunkName = file.name + '-' + currentChunk;
      formData.append('file', file.slice(start, end), chunkName);
      formData.append('index', currentChunk.toString());
      formData.append('totalChunks', chunks.toString());
      this.msFilesService.upload(formData).subscribe(
        response => {
          console.log(response);
          currentChunk++;
          if (currentChunk < chunks) {
            uploadFileChunk(file, currentChunk * chunkSize, (currentChunk + 1) * chunkSize);
          } else {
            console.log('Todos los chunks se han subido correctamente');
            this.msFilesService.merge(fileToUpload.name)
              .pipe(finalize(() => {
                this.uploading = false;
                this.blocked = false;
              }))
              .subscribe(
                response => {
                  console.log('data:image/jpeg;base64,'+ response.img);
                  this.artsToLoad.push(<LoadArtDto>{
                    fileName: fileToUpload.name,
                    file: 'data:image/jpeg;base64,'+ response.img ,
                    type: 'image/jpeg',
                    size: fileToUpload.size,
                    sizeSmall: response.size,
                    isBigImageSize: true
                  });
                  this.changes.detectChanges();
                },
                error => {
                  console.log(error);
                }
              );
          }
        },
        error => {
          this.msgService.showError(error, "Error subiendo imagen");
        }
      );
    };
    uploadFileChunk(fileToUpload, 0, chunkSize);
  }

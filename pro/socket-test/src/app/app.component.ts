import { Component, OnInit } from '@angular/core';
import { NotificationService } from './services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'socket-test';

  constructor(private readonly notificationService:NotificationService) {
  }

  ngOnInit(): void {
    this.notificationService.startConnection();
  }

  sendHub(){
    this.notificationService.sendMessage("Roco", "I'm here my boy...");
  }

}

import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private hubConnection!: signalR.HubConnection;

  constructor() { }

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:7234/sockethub',{
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
      }) // URL del hub SignalR
      .build();

    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started');
      })
      .catch(err => console.error('Error while starting connection: ' + err));
  }

  public addMessageListener = (callback: (user: string, message: string) => void) => {
    this.hubConnection.on('ReceiveMessage', callback);
  }

  public sendMessage = (user: string, message: string) => {
    this.hubConnection.invoke('SendMessage', user, message);
  }

  public joinGroup = (groupId: string) => {
    this.hubConnection.invoke('JoinGroup', groupId);
  }

  public sendMessageToGroup = (groupId: string, user: string, message: string) => {
    this.hubConnection.invoke('SendMessageToGroup', groupId, user, message);
  }
}

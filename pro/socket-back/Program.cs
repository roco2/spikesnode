using Microsoft.AspNetCore.SignalR;
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddSignalR();
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "cors",
                      builder =>
                      {
                          builder.WithOrigins("http://localhost:4200");
                          builder.AllowAnyHeader();
                          builder.AllowAnyMethod();
                      });
});
var app = builder.Build();
app.UseCors("cors");
app.MapGet("/", () => "SignalR Test.....!!!!!!");
app.MapHub<SocketHub>("/sockethub");
app.Run();


public class SocketHub : Hub
{
    public async Task SendMessage(string user, string message)
    {
        await Clients.All.SendAsync("ReceiveMessage", user, message);
    }

    public async Task JoinGroup(string groupId)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId, groupId);
    }

    public async Task SendMessageToGroup(string groupId, string user, string message)
    {
        await Clients.Group(groupId).SendAsync("ReceiveMessage", user, message);
    }

    public async Task SendMessageToUser(string userId, string user, string message)
    {
        if (Clients.Client(userId) != null)
        {
            await Clients.Client(userId).SendAsync("ReceiveMessage", user, message);
        }
    }

    public override async Task OnConnectedAsync()
    {
        var c = Context.ConnectionId;
        await base.OnConnectedAsync();
    }

    public override async Task OnDisconnectedAsync(Exception exception)
    {
        await base.OnDisconnectedAsync(exception);
    }
}
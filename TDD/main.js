class CalculadorArea {
    calcularAreaCuadrado(a) {
        if (a < 1) {
            return 'Error el resultado debe ser mayor a 0';
        }
        let result = a * a;
        return result;
    }

    calcularAreaRectangulo(a, b) {
        let result = a * b;
        return result;
    }

    calcularAreaTriangulo(a, b) {
        let result = (a * b)/2;
        return result;
    }
}

module.exports = CalculadorArea;
/*
Calcular area de un cuadrado  de 5 x 5 como resultado 25
Calcular area de un cuadrado  de 2 x 2 como resultado 4
Calcular area de un rectangulo de 2 x 4 como resultado 8
Calcular area de un triangulo  de altura 12 y base 20 como resultado 120
*/

const { it, expect } = require ("@jest/globals");
const CalculadorArea = require('./main');

describe('caluculador de areas', ()=>{
    const calculadorArea  = new CalculadorArea();
    it('calcular el area de un cuadrado de 5 x 5', () =>{
        const  resultado = calculadorArea.calcularAreaCuadrado(5);
        expect(resultado).toBe(25);
    });

    it('Calcular area de un cuadrado  de 2 x 2', () =>{
        const  resultado = calculadorArea.calcularAreaCuadrado(2);
        expect(resultado).toBe(4);
    });

    it('calucular el area de un cuadrado de 0 x 0 mensaje de error', () =>{
        const  resultado = calculadorArea.calcularAreaCuadrado(0);
        expect(resultado).toBe('Error el resultado debe ser mayor a 0');
    });

    it('Calcular area de un rectangulo  de 2 x 4', () =>{
        const  resultado = calculadorArea.calcularAreaRectangulo(2, 4);
        expect(resultado).toBe(8);
    });

    it('Calcular area de un triangulo  de 12 x 20', () =>{
        const  resultado = calculadorArea.calcularAreaTriangulo(12, 20);
        expect(resultado).toBe(120);
    });
});
﻿namespace Epm.Ccd.CarpetaCiudadana.Data.Services
{
    using Epm.Ccd.CarpetaCiudadana.Data.Interfaces;
    using Epm.Ccd.CarpetaCiudadana.Modelo;
    using Epm.Ccd.CarpetaCiudadana.Modelo.Models;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    public class ServiciosCrm : IServiciosCrm
    {
        private readonly IOptions<ConfiguracionCrmApi> ConfigurationApiCrm;
        private HttpClient clientehttp { get; set; }
        private readonly IServiciosAuthorization _serviciosAuthorization;

        public ServiciosCrm(IOptions<ConfiguracionCrmApi> configurationApiCrm, IServiciosAuthorization serviciosAuthorization)
        {
            ConfigurationApiCrm = configurationApiCrm;
            _serviciosAuthorization = serviciosAuthorization;
            InicializarServicios();
        }
        public ServiciosCrm(IOptions<ConfiguracionCrmApi> configurationApiCrm, IServiciosAuthorization serviciosAuthorization, HttpClient Clientehttp)
        {
            ConfigurationApiCrm = configurationApiCrm;
            _serviciosAuthorization = serviciosAuthorization;
            InicializarServicios();
            this.clientehttp = Clientehttp;
        }

        public ServiciosCrm()
        {
        }

        private void InicializarServicios()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        }

        public async Task<string> TransaccionCrm(ConectorCrmEjecutarAccionPeticion conectorCrmEjecutarAccionPeticion)
        {
            string urlAccionCRM = $"{ConfigurationApiCrm.Value.UrlServicio}EjecutarAccion";
            JObject parametros = new JObject
                {
                    { "NombreAccion", conectorCrmEjecutarAccionPeticion.NombreAccion }
                };

            if (conectorCrmEjecutarAccionPeticion.Parametros != null && conectorCrmEjecutarAccionPeticion.Parametros.Count > 0)
            {
                parametros.Add("Parametros", JArray.Parse(JsonConvert.SerializeObject(conectorCrmEjecutarAccionPeticion.Parametros)));
            }

            if (conectorCrmEjecutarAccionPeticion.Target != null && conectorCrmEjecutarAccionPeticion.Target.Count > 0)
            {
                parametros.Add("Target", JArray.Parse(JsonConvert.SerializeObject(conectorCrmEjecutarAccionPeticion.Target)));
            }

            parametros.Add("Instancia", ConfigurationApiCrm.Value.instanciaEjecutarAccion);

            HttpClientHandler clientHandler = new HttpClientHandler
            {
            };

            if (this.clientehttp == null)
            {
                this.clientehttp = new HttpClient(clientHandler);
            }

            this.clientehttp.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", ConfigurationApiCrm.Value.key);
            this.clientehttp.DefaultRequestHeaders.Add("X-SessionId", ConfigurationApiCrm.Value.sessionId);

            await AsignarToken();

            HttpContent Content = new StringContent(parametros.ToString(), Encoding.UTF8, "application/json");

            var response = await this.clientehttp.PostAsync(urlAccionCRM, Content);
            string resultadoJson = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode && resultadoJson != null)
            {
                this.clientehttp.Dispose();
                this.clientehttp = null;
                return resultadoJson;
            }
            else
            {
                RespuestaConsultaPqrs respuesta = new RespuestaConsultaPqrs();
                respuesta.Respuesta = new List<PqrsAccion>();
                respuesta.Error = $"Ocurrió un error al ejecutar una consulta en CRM";
                return JsonConvert.SerializeObject(respuesta);
            }
        }

        private async Task AsignarToken()
        {
            var oauthRequest = new OAuthRequest()
            {
                ClientId = ConfigurationApiCrm.Value.OAuthClientId,
                ClientSecret = ConfigurationApiCrm.Value.OAuthClientSecret,
                Scope = ConfigurationApiCrm.Value.OAuthScope
            };
            var authToken = await _serviciosAuthorization.ObtenerAuthorizationToken(oauthRequest);

            this.clientehttp.DefaultRequestHeaders.Add("Authorization", authToken);
        }
    }
}

﻿namespace Epm.Ccd.CarpetaCiudadana.Data.Services
{
    using Epm.Ccd.CarpetaCiudadana.Data.Interfaces;
    using Epm.Ccd.CarpetaCiudadana.Modelo.Models;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class ServiciosAuthorization : IServiciosAuthorization
    {
        private readonly IConfiguration _configuration;
        private HttpClient _httpClient;

        public ServiciosAuthorization(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ServiciosAuthorization(IConfiguration configuration, HttpClient httpClient)
        {
            _configuration = configuration;
            _httpClient = httpClient;
        }

        public async Task<string> ObtenerAuthorizationToken(OAuthRequest request)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();

            if (_httpClient == null)
            {
                _httpClient = new HttpClient(clientHandler);
            }

            var body = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("client_id", request.ClientId),
                new KeyValuePair<string, string>("client_secret",request.ClientSecret),
                new KeyValuePair<string, string>("scope", request.Scope),
                new KeyValuePair<string, string>("grant_type", _configuration["AuthorizationBase:GrantType"])
            };

            var response = await _httpClient.PostAsync(_configuration["AuthorizationBase:BaseApi"], new FormUrlEncodedContent(body));
            string resultadoJson = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode && resultadoJson != null)
            {
                _httpClient.Dispose();
                _httpClient = null;

                var respuestaToken = JsonConvert.DeserializeObject<OAuthResponse>(resultadoJson);

                if (!string.IsNullOrEmpty(respuestaToken.TokenType) && !string.IsNullOrEmpty(respuestaToken.AccessToken))
                {
                    return respuestaToken.TokenType + " " + respuestaToken.AccessToken;
                }
                else
                {
                    throw new ArgumentException($"No se pudo obtener la respuesta del servicio de autenticación: {response.StatusCode}");
                }
            }
            else
            {
                throw new ArgumentException($"Ocurrió un error al consumir el servicio de autenticación: {response.StatusCode}");
            }
        }
    }
}

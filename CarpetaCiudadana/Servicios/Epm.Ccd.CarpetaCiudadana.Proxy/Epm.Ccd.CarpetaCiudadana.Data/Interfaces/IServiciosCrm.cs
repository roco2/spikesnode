﻿using Epm.Ccd.CarpetaCiudadana.Modelo;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Epm.Ccd.CarpetaCiudadana.Data.Interfaces
{
    public interface IServiciosCrm
    {
        Task<string> TransaccionCrm(ConectorCrmEjecutarAccionPeticion conectorCrmEjecutarAccionPeticion);
    }
}

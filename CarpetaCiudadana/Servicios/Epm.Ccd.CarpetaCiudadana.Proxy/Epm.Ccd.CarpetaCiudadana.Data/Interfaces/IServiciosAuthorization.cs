﻿namespace Epm.Ccd.CarpetaCiudadana.Data.Interfaces
{
    using Epm.Ccd.CarpetaCiudadana.Modelo.Models;
    using System.Threading.Tasks;

    public interface IServiciosAuthorization
    {
        Task<string> ObtenerAuthorizationToken(OAuthRequest request);
    }
}

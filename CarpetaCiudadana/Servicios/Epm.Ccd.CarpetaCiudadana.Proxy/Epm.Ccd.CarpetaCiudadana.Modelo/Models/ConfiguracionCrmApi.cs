﻿namespace Epm.Ccd.CarpetaCiudadana.Modelo
{
    public class ConfiguracionCrmApi
    {
        public string Nombre { get; set; }
        public string UrlServicio { get; set; }
        public string key { get; set; }
        public string sessionId { get; set; }
        public string instanciaEjecutarAccion { get; set; }
        public string OAuthClientId { get; set; }
        public string OAuthClientSecret { get; set; }
        public string OAuthScope { get; set; }
    }
}

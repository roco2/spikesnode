﻿using System.Collections.Generic;

namespace Epm.Ccd.CarpetaCiudadana.Modelo
{
    public class RespuestaConsultaPqrs
    {
        public List<PqrsAccion> Respuesta { get; set; }
        public object Error { get; set; }
    }
}

﻿using System.Collections.Generic;
namespace Epm.Ccd.CarpetaCiudadana.Modelo
{
    public class ConectorCrmEjecutarAccionPeticion
    {
        public string NombreAccion { get; set; }
        public List<Parametro> Parametros { get; set; }
        public List<string> Target { get; set; }
    }
}

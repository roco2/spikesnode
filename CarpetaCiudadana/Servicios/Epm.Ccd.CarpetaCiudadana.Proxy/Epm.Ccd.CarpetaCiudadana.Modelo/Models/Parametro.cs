﻿namespace Epm.Ccd.CarpetaCiudadana.Modelo
{
    public class Parametro
    {
        public string Key { get; set; }
        public object Value { get; set; }
    }
}

﻿namespace Epm.Ccd.CarpetaCiudadana.Modelo
{
    public class EntidadConsultada
    {
        public string nomEntidad { get; set; }
        public string fechaConsulta { get; set; }
    }
}

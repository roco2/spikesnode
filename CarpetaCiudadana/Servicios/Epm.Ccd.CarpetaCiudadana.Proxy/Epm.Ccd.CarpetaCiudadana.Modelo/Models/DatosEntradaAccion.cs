﻿namespace Epm.Ccd.CarpetaCiudadana.Modelo
{
    public class DatosEntradaAccion
    {
        public string NumeroIdentificacion { get; set; }
        public string TipoIdentificacion { get; set; }
    }
}

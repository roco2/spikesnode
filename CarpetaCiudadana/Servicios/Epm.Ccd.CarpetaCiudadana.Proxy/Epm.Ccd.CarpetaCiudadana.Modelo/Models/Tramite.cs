﻿using System.Collections.Generic;
namespace Epm.Ccd.CarpetaCiudadana.Modelo
{
    public class Tramite
    {
        public string idTramiteEntidad { get; set; }
        public string nomTramiteGenerado { get; set; }
        public string fechaRealizaTramiteUsuario { get; set; }
        public string servicioConsulta { get; set; }
        public string estadoTramiteUsuario { get; set; }
        public List<EntidadConsultada> entidadConsultada { get; set; }
        
    }
}

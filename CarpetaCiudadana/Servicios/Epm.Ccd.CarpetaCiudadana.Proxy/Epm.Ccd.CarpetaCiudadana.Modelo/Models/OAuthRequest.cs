﻿namespace Epm.Ccd.CarpetaCiudadana.Modelo.Models
{
    public class OAuthRequest
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Scope { get; set; }
    }
}

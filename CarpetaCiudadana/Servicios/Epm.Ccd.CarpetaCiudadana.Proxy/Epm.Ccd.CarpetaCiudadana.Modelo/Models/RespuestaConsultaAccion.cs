﻿namespace Epm.Ccd.CarpetaCiudadana.Modelo
{
    public class RespuestaConsultaAccion
    {
        public RespuestaJson Respuesta { get; set; }
        public string Error { get; set; }
    }
}

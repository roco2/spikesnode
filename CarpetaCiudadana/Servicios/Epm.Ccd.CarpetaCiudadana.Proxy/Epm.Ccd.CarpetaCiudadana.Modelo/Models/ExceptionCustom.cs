﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Epm.Ccd.CarpetaCiudadana.Modelo.Models
{
    public class ExceptionCustom : Exception
    {
        public string CustomMessage { get; set; }
        public string MessageError { get; set; }

        public ExceptionCustom(string customMessage, string messageError)
            : base(customMessage)
        {
            CustomMessage = customMessage;
            MessageError = messageError;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Epm.Ccd.CarpetaCiudadana.Modelo.Constants
{
    public  class HelperConstants
    {
        public const string ContenType = "Content-Type";
        public const string Encoding = "application/json; charset=utf-8";
        public const string EncodingError = "application/problem+json; charset=utf-8";
        public const string ValueContentType = "application/json";
    }
}

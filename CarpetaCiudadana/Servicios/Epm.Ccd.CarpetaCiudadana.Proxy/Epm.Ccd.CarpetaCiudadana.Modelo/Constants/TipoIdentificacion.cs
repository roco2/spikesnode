﻿namespace Epm.Ccd.CarpetaCiudadana.Modelo.Constants
{
    public class TipoIdentificacionValue
    {
        public const string Cedula = "919900000";
        public const string CedulaExtranjeria = "919900001";
        public const string Pasaporte = "919900002";
    }
    public class TipoIdentificacionName
    {
        public const string Cedula = "CC";
        public const string CedulaExtranjeria = "CE";
        public const string Pasaporte = "PA";
    }
}

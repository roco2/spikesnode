﻿using Epm.Ccd.CarpetaCiudadana.Modelo;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Epm.Ccd.CarpetaCiudadana.Negocio.Interfaces
{
    public interface INegocioConsultasPqrs
    {
        Task<JObject> ResolverConsultaPqrs(DatosEntrada datosEntrada);
    }
}

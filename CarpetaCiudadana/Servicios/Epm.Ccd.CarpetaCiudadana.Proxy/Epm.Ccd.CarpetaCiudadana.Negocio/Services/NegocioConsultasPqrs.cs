﻿using Epm.Ccd.CarpetaCiudadana.Data.Interfaces;
using Epm.Ccd.CarpetaCiudadana.Modelo;
using Epm.Ccd.CarpetaCiudadana.Modelo.Constants;
using Epm.Ccd.CarpetaCiudadana.Modelo.Models;
using Epm.Ccd.CarpetaCiudadana.Negocio.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Epm.Ccd.CarpetaCiudadana.Negocio.Services
{
    public class NegocioConsultasPqrs : INegocioConsultasPqrs
    {
        private readonly IServiciosCrm ServiciosCrm;
        private readonly IConfiguration Configuration;

        public NegocioConsultasPqrs(IServiciosCrm transaccionCrmBase, IConfiguration configuration)
        {
            ServiciosCrm = transaccionCrmBase;
            Configuration = configuration;
        }

        public async Task<JObject> ResolverConsultaPqrs(DatosEntrada datosEntrada)
        {

            object validacion = ValidarDatosEntrada(datosEntrada).SelectToken("Error");
            if (string.IsNullOrEmpty(validacion.ToString()))
            {
                ConectorCrmEjecutarAccionPeticion conectorCrmEjecutarAccionPeticion = new ConectorCrmEjecutarAccionPeticion()
                {
                    NombreAccion = Configuration["AccionConsultaPqrs"]
                };
                conectorCrmEjecutarAccionPeticion.Parametros = this.MapearParametros(datosEntrada);
                string respuestaServicio = await ServiciosCrm.TransaccionCrm(conectorCrmEjecutarAccionPeticion);
                return MapearDatosRespuesta(respuestaServicio);
            }
            else
            {
                return SerializarRespuesta(ValidarDatosEntrada(datosEntrada));
            }
        }

        private JObject ValidarDatosEntrada(DatosEntrada datosEntrada)
        {
            string Error = string.Empty;
            if (datosEntrada == null)
            {
                Error = Error + "No se han parámetros en la petición.";
            }
            if (string.IsNullOrEmpty(datosEntrada.idUsuario))
            {
                Error = Error + "No se ha encontrado un valor para el campo idUsuario.";
            }
            if (string.IsNullOrEmpty(datosEntrada.tipoId))
            {
                Error = Error + "No se ha encontrado un valor para el campo tipoId.";
            }
            if (!ValidarTipoIdentificacion(datosEntrada.tipoId))
            {
                Error = Error + "El tipo de identificación no es válido";
            }
            return MapearErrorRespuesta(Error);

        }

        private bool ValidarTipoIdentificacion(string tipoIdentificacion)
        {
            bool valido = false;
            if (tipoIdentificacion == TipoIdentificacionName.Cedula ||
                tipoIdentificacion == TipoIdentificacionName.CedulaExtranjeria ||
                tipoIdentificacion == TipoIdentificacionName.Pasaporte)
            {
                valido = true;
            }
            return valido;
        }

        private JObject MapearDatosRespuesta(string respuestaServicio)
        {
            RespuestaConsultaAccion datos = JsonConvert.DeserializeObject<RespuestaConsultaAccion>(respuestaServicio);
            RespuestaConsultaPqrs datosPqrs = JsonConvert.DeserializeObject<RespuestaConsultaPqrs>(datos.Respuesta.JsonRespuesta);

            if (datosPqrs.Error == null)
            {
                RespuestaTramite tramites = new RespuestaTramite();
                tramites.tramiteUsuarioEntidad = MapearTramites(datosPqrs);
                return SerializarRespuesta(tramites);
            }
            else
            {
                return MapearErrorRespuesta(datosPqrs.Error.ToString());
            }
        }

        private JObject MapearErrorRespuesta(string error)
        {
            RespuestaConsultaPqrs respuesta = new RespuestaConsultaPqrs()
            {
                Error = error,
                Respuesta = new List<PqrsAccion>()
            };
            return SerializarRespuesta(respuesta);
        }

        private JObject SerializarRespuesta(object respuesta)
        {
            return JObject.Parse(JsonConvert.SerializeObject(respuesta));
        }

        private List<Tramite> MapearTramites(RespuestaConsultaPqrs datosPqrs)
        {
            List<Tramite> listadoTramites = new List<Tramite>();
            EntidadConsultada entidad = new EntidadConsultada();
            entidad.nomEntidad = "";
            entidad.fechaConsulta = "";
            foreach (PqrsAccion pqr in datosPqrs.Respuesta)
            {
                Tramite tramite = new Tramite();
                tramite.idTramiteEntidad = pqr.NumeroCaso;
                tramite.nomTramiteGenerado = pqr.TipoCaso;
                tramite.fechaRealizaTramiteUsuario = string.IsNullOrEmpty(pqr.FechaRadicado) ? pqr.FechaCreacion : pqr.FechaRadicado;
                tramite.servicioConsulta = pqr.TipoCaso;
                tramite.estadoTramiteUsuario = pqr.EstadoCaso.Replace("Pendiente", "Pend").Replace("Notificación", "Notificar");
                tramite.entidadConsultada = new List<EntidadConsultada>();
                tramite.entidadConsultada.Add(entidad);
                listadoTramites.Add(tramite);
            }
            if (datosPqrs.Respuesta.Count == 0)
            {
                var tramite = new Tramite()
                {
                    idTramiteEntidad = "",
                    estadoTramiteUsuario = "",
                    fechaRealizaTramiteUsuario = "",
                    nomTramiteGenerado = "",
                    servicioConsulta = "",
                    entidadConsultada = new List<EntidadConsultada>() { entidad }
                };
                listadoTramites.Add(tramite);
            }

            return listadoTramites;
        }

        private List<Parametro> MapearParametros(DatosEntrada datosEntrada)
        {
            DatosEntradaAccion datosAccion = new DatosEntradaAccion();
            datosAccion.NumeroIdentificacion = datosEntrada.idUsuario;
            datosAccion.TipoIdentificacion = MapearTipoIdentificacion(datosEntrada.tipoId);

            List<Parametro> parametros = new List<Parametro>() {
                new Parametro()
                {
                    Key = "JsonPeticion",
                    Value =  JsonConvert.SerializeObject(datosAccion)
                }
            };
            return parametros;
        }

        private string MapearTipoIdentificacion(string tipoId)
        {
            switch (tipoId)
            {
                case TipoIdentificacionName.Cedula:
                    return TipoIdentificacionValue.Cedula;
                case TipoIdentificacionName.CedulaExtranjeria:
                    return TipoIdentificacionValue.CedulaExtranjeria;
                case TipoIdentificacionName.Pasaporte:
                    return TipoIdentificacionValue.Pasaporte;
                default:
                    return string.Empty;
            }
        }
    }

}

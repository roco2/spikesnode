﻿using Microsoft.AspNetCore.Builder;
namespace Epm.Ccd.CarpetaCiudadana
{
    /// <summary>
    /// 
    /// </summary>
    public static class ExceptionMiddlewareExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="isDevelopment"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseExceptionMiddleware(this IApplicationBuilder builder, bool isDevelopment)
        {
            return builder.UseMiddleware<ExceptionMiddleware>(isDevelopment);
        }
    }
}

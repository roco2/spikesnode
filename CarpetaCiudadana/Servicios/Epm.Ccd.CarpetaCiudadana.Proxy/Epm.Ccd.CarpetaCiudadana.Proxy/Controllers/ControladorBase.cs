﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Epm.Ccd.CarpetaCiudadana.Controllers
{
    /// <summary>
    /// Controlador ControladorBase
    /// </summary>
    public class ControladorBase : ControllerBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ControladorBase()
        {
        }
        /// <summary>
        /// Método que permite retornar una respuesta parseada
        /// </summary>        
        /// <param name="respuesta"></param>
        /// <returns></returns>
        protected JObject RetornarRespuesta(string respuesta)
        {
            return JObject.Parse(respuesta);
        }
    }
}

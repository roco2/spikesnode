﻿using Epm.Ccd.CarpetaCiudadana.Modelo;
using Epm.Ccd.CarpetaCiudadana.Negocio.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Epm.Ccd.CarpetaCiudadana.Controllers
{
    /// <summary>
    /// PqrsController
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class PqrsController : ControladorBase
    {
        private readonly INegocioConsultasPqrs NegocioConsultasPqrs;

        /// <summary>
        /// Constructor
        /// </summary>
        public PqrsController(INegocioConsultasPqrs negocioConsultasPqrs)
        {
            NegocioConsultasPqrs = negocioConsultasPqrs;
        }

        /// <summary>
        /// Método que permite consultas las pqrs de un usuario
        /// </summary>
        /// <returns></returns>
        [HttpPost("ConsultaPqrs")]
        public async Task<IActionResult> ConsultaPqrs([FromBody] DatosEntrada datosEntrada)
        {
            JObject respuesta = await NegocioConsultasPqrs.ResolverConsultaPqrs(datosEntrada);
            object validacion = respuesta.SelectToken("Error");
            if (validacion == null)
            {
                return Ok(respuesta);
            }
            else
            {
                return Problem(validacion.ToString(), respuesta.ToString(), statusCode: (int)HttpStatusCode.BadRequest);
            }
        }

        [HttpGet("ConsultaPqrs/{tipoId}/{idUsuario}")]
        public async Task<IActionResult> ConsultaPqrsnew(string tipoId, string idUsuario)
        {
            var datosEntrada = new DatosEntrada() { tipoId = tipoId, idUsuario = idUsuario};
            JObject respuesta = await NegocioConsultasPqrs.ResolverConsultaPqrs(datosEntrada);
            object validacion = respuesta.SelectToken("Error");
            if (validacion == null)
            {
                return Ok(respuesta);
            }
            else
            {
                return Problem(validacion.ToString(), statusCode: (int)HttpStatusCode.BadRequest);
            }
        }

    }
}

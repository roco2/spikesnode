﻿using Epm.Ccd.CarpetaCiudadana.Modelo.Constants;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Runtime.InteropServices;
using System.Text.Unicode;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

public class HelperMiddleware
{
    private readonly RequestDelegate _next;
    public HelperMiddleware(RequestDelegate next)
    {
        _next = next;
    }
    public async Task Invoke(HttpContext context)
    {
        RemoveHeader(context);
        await _next.Invoke(context);
    }

    public void RemoveHeader(HttpContext context)
    {
        context.Response.OnStarting((state) =>
        {
            if (context.Response.Headers.Count > 0 && context.Response.Headers.ContainsKey(HelperConstants.ContenType))
        {
            var contentType = context.Response.Headers[HelperConstants.ContenType].ToString();
            if (contentType.StartsWith(HelperConstants.Encoding) || contentType.StartsWith(HelperConstants.EncodingError))
                {
            context.Response.Headers.Remove(HelperConstants.ContenType);
                    context.Response.Headers.Append(HelperConstants.ContenType, HelperConstants.ValueContentType);
                }
            }

            return Task.FromResult(0);
        }, null);

    }
}

public static class MiddlewareExtension
{
    public static IApplicationBuilder UseCustomMiddleware(this IApplicationBuilder builder)
    => builder.UseMiddleware<HelperMiddleware>();
}
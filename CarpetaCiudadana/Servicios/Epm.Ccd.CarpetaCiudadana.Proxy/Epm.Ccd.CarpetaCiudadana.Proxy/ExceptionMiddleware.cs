﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Epm.Ccd.CarpetaCiudadana
{
    /// <summary>
    /// 
    /// </summary>
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate nextMiddleware;
        private readonly bool isDevelopment;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nextMiddleware"></param>
        /// <param name="isDevelopment"></param>
        public ExceptionMiddleware(RequestDelegate nextMiddleware, bool isDevelopment)
        {
            this.nextMiddleware = nextMiddleware;
            this.isDevelopment = isDevelopment;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext httpContext)
        {
            IExceptionHandlerFeature exceptionDetails = httpContext.Features.Get<IExceptionHandlerFeature>();
            Exception exception = exceptionDetails?.Error;


            if (exception != null)
            {
                string traceHeader = string.Empty;
                if (httpContext.Request.Headers.TryGetValue("trace-error", out var traceValue))
                {
                    traceHeader = traceValue;
                }

                httpContext.Response.ContentType = "application/json";

                string title = "An error occured";
                string details = null;

                if (isDevelopment)
                {
                    title += $": {exception.Message}";
                    details = exception.ToString();
                }

                var problem = new ProblemDetails
                {
                    Status = (int)HttpStatusCode.InternalServerError,
                    Title = title,
                    Detail = details
                };

                if (exception is Exception ex)
                {
                    problem.Detail = traceHeader == "true" ? ex.Message + ex.StackTrace + ex.TargetSite : null;
                }

                var traceId = Activity.Current?.Id ?? httpContext?.TraceIdentifier;
                if (traceId != null)
                {
                    problem.Extensions["traceId"] = traceId;
                }

                var stream = httpContext.Response.Body;
                await JsonSerializer.SerializeAsync(stream, problem);


                await nextMiddleware(httpContext);
            }
        }
    }
}


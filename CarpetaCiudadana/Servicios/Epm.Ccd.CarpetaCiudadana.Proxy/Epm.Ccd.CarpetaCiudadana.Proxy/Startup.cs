using Epm.Ccd.CarpetaCiudadana.Data.Interfaces;
using Epm.Ccd.CarpetaCiudadana.Data.Services;
using Epm.Ccd.CarpetaCiudadana.Modelo;
using Epm.Ccd.CarpetaCiudadana.Negocio.Interfaces;
using Epm.Ccd.CarpetaCiudadana.Negocio.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Epm.Ccd.CarpetaCiudadana
{
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// 
        /// </summary>
        protected readonly SwaggerConfiguration swaggerConfiguration;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            swaggerConfiguration = new SwaggerConfiguration(Configuration);
        }
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                builder =>
                {
                    builder.WithOrigins(Configuration.GetSection("ConfigurationApplication:" + "CorsPolicyOrigins").Value.Split('|'))
                            .AllowAnyHeader()
                            .AllowAnyMethod();

                });

            });

            services.AddControllers()
                   .AddNewtonsoftJson()
                   .AddJsonOptions(options => options.JsonSerializerOptions.IgnoreNullValues = true)
                   .AddJsonOptions(options => options.JsonSerializerOptions.MaxDepth = int.MaxValue)
                   .ConfigureApiBehaviorOptions(option =>
                   {
                       option.InvalidModelStateResponseFactory = actionContext =>
                       {
                           var modalState = actionContext.ModelState.Values;
                           return new BadRequestObjectResult(modalState);
                       };
                   });

            services.Configure<ConfiguracionCrmApi>(options =>
            {
                options.UrlServicio = Configuration["ApiCRM:baseApi"];
                options.key = Configuration["ApiCRM:subscriptionKey"];
                options.sessionId = Configuration["ApiCRM:sessionId"];
                options.instanciaEjecutarAccion = Configuration["ApiCRM:instanciaEjecutarAccion"];
                options.OAuthClientId = Configuration["AuthorizationApiCRM:ClientId"];
                options.OAuthClientSecret = Configuration["AuthorizationApiCRM:ClientSecret"];
                options.OAuthScope = Configuration["AuthorizationApiCRM:Scope"];
            });

            services.AddScoped<IServiciosCrm, ServiciosCrm>();
            services.AddScoped<INegocioConsultasPqrs, NegocioConsultasPqrs>();
            services.AddScoped<IServiciosAuthorization, ServiciosAuthorization>();

            AddSwagger(services);

            // enable Application Insights telemetry
            services.AddApplicationInsightsTelemetry(Configuration["ApplicationInsights:InstrumentationKey"]);

            services.AddTransient<HelperMiddleware>();
            services.AddHealthChecks();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseExceptionHandler(err => err.UseExceptionMiddleware(env.EnvironmentName == "dllo"));
            app.UseCustomMiddleware();
            app.UseResponseCompression();
            app.UseCors("CorsPolicy");
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseStaticFiles();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/healh", new HealthCheckOptions
                {
                    AllowCachingResponses = false
                });
            });
            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(swaggerConfiguration.EndpointSwaggerJson, swaggerConfiguration.EndpointDescription);
                options.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
            });

        }

        /// <summary>
        /// 
        /// </summary>
        protected OpenApiInfo GetApiInfo => new OpenApiInfo
        {
            Title = swaggerConfiguration.DocInfoTitle,
            Version = swaggerConfiguration.DocInfoVersion,
            Description = swaggerConfiguration.DocInfoDescription,
            Contact = GetApiContact
        };

        /// <summary>
        /// 
        /// </summary>
        protected OpenApiContact GetApiContact => new OpenApiContact
        {
            Name = swaggerConfiguration.ContactName,
            Url = swaggerConfiguration.ContactUrl,
            Email = swaggerConfiguration.ContactEmail
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        protected void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(swagger =>
            {
                swagger.DescribeAllParametersInCamelCase();

                swagger.AddSecurityDefinition("Ocp-Apim-Subscription-Key", new OpenApiSecurityScheme
                {
                    Description = @"Api key needed to access the endpoints. Ocp-Apim-Subscription-Key: My_API_Key",
                    In = ParameterLocation.Header,
                    Name = "Ocp-Apim-Subscription-Key",
                    Type = SecuritySchemeType.ApiKey
                });

                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Name = "Ocp-Apim-Subscription-Key",
                            Type = SecuritySchemeType.ApiKey,
                            In = ParameterLocation.Header,
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Ocp-Apim-Subscription-Key"
                            },
                         },
                         new List<string>()
                     }
                });

                swagger.SwaggerDoc(swaggerConfiguration.DocInfoVersion, GetApiInfo);

                swagger.DocInclusionPredicate((docName, description) => true);
                swagger.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());

                var xmlPath = Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
                swagger.IncludeXmlComments(xmlPath);
            });
        }
    }
}

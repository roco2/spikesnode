namespace Epm.Ccd.CarpetaCiudadana.Test
{
    using Epm.Ccd.CarpetaCiudadana.Controllers;
    using Epm.Ccd.CarpetaCiudadana.Data.Interfaces;
    using Epm.Ccd.CarpetaCiudadana.Data.Services;
    using Epm.Ccd.CarpetaCiudadana.Modelo;
    using Epm.Ccd.CarpetaCiudadana.Negocio.Interfaces;
    using Epm.Ccd.CarpetaCiudadana.Negocio.Services;
    using Moq;
    using Moq.Protected;
    using NUnit.Framework;
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    [TestFixture]
    public class PqrsControllerTest
    {
        private IServiciosAuthorization InstanceServiciosAuthorization()
        {
            var jsonResponseToken = "{\"token_type\":\"Bearer\",\"expires_in\":3599,\"ext_expires_in\":3599,\"access_token\":\"eyJ0eXAiOiJUAxb3Bw\"}";

            Mock<HttpMessageHandler> handlerAuthorizationMock = new Mock<HttpMessageHandler>();
            handlerAuthorizationMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(jsonResponseToken, Encoding.UTF8, "application/json")
                })
                .Verifiable();

            var httpClientAuthorization = new HttpClient(handlerAuthorizationMock.Object)
            {
                BaseAddress = new Uri("http://testAuthorization.com/")
            };

            IServiciosAuthorization serviciosAuthorization = new ServiciosAuthorization(SetUpClass._configuration, httpClientAuthorization);
            return serviciosAuthorization;
        }

        [Test]
        public async Task EjecutarControladorConsultaPqrs()
        {
            DatosEntrada datosEntrada = new DatosEntrada
            {
                tipoId = "919900001",
                idUsuario = "234234"
            };
            string respuesta = "{\"Respuesta\":{\"JsonRespuesta\":\"{\\\"Respuesta\\\":[{\\\"NumeroCaso\\\":\\\"PQR-8975488-L6W1\\\",\\\"FechaCreacion\\\":\\\"2021-10-07 11:15:08\\\",\\\"EstadoCaso\\\":\\\"En Trámite\\\",\\\"TipoCaso\\\":\\\"Petición\\\",\\\"Oficina\\\":\\\"Edificio EPM\\\",\\\"FechaRadicado\\\":\\\"2021-10-07 00:00:00\\\",\\\"Causa\\\":\\\"Información general\\\",\\\"Motivo\\\":\\\"Prueba relación pps contacto roll usuario desde la web. Servicios seleccionados: Agua potable,\\\",\\\"FechaLimiteRespuesta\\\":\\\"2021-10-28 23:59:59\\\"}],\\\"Error\\\":null}\"},\"Error\":\"\"}";
            HttpResponseMessage httpResponse = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(respuesta, Encoding.UTF8, "application/json")
            };
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .SetupSequence<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(httpResponse)
                .ReturnsAsync(httpResponse);

            var client = new HttpClient(mockHttpMessageHandler.Object);

            IServiciosCrm serviciosCrm = new ServiciosCrm(SetUpClass._configurationApiCrm, InstanceServiciosAuthorization(), client);
            INegocioConsultasPqrs negocioConsultasPqrs = new NegocioConsultasPqrs(serviciosCrm, SetUpClass._configuration);
            var controller = new PqrsController(negocioConsultasPqrs);
            //JObject respuestaControlador = await controller.ConsultaPqrs(datosEntrada);
            //JArray respuestaArray = (JArray)respuestaControlador.SelectToken("tramiteUsuarioEntidad");
            //Assert.Greater(respuestaArray.Count, 0);
        }

        [Test]
        public async Task EjecutarControladorConsultaPqrsError()
        {
            DatosEntrada datosEntrada = new DatosEntrada
            {
                tipoId = "919900001123123",
                idUsuario = "234234"
            };
            string respuesta = "{\"Respuesta\":{\"JsonRespuesta\":\"{\\\"Respuesta\\\":null,\\\"Error\\\":\\\"El tipo de identificación no es válido.\\\"}\"},\"Error\":\"\"}";
            HttpResponseMessage httpResponse = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(respuesta, Encoding.UTF8, "application/json")
            };
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();
            mockHttpMessageHandler.Protected()
                .SetupSequence<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(httpResponse)
                .ReturnsAsync(httpResponse);

            var client = new HttpClient(mockHttpMessageHandler.Object);
            
            IServiciosCrm serviciosCrm = new ServiciosCrm(SetUpClass._configurationApiCrm, InstanceServiciosAuthorization(), client);
            INegocioConsultasPqrs negocioConsultasPqrs = new NegocioConsultasPqrs(serviciosCrm, SetUpClass._configuration);
            var controller = new PqrsController(negocioConsultasPqrs);
            //JObject respuestaControlador = await controller.ConsultaPqrs(datosEntrada);
            //string error = (string)respuestaControlador.SelectToken("Error");
            //Assert.AreEqual(error, "El tipo de identificación no es válido.");
        }
    }
}
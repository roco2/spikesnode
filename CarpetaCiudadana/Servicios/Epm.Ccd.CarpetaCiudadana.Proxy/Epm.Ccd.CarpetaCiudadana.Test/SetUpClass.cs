﻿namespace Epm.Ccd.CarpetaCiudadana.Test
{
    using Epm.Ccd.CarpetaCiudadana.Modelo;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using NUnit.Framework;
    using System.Collections.Generic;

    [SetUpFixture]
    public class SetUpClass
    {
        public static IOptions<ConfiguracionCrmApi> _configurationApiCrm;
        public static IConfiguration _configuration;

        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            var crmApi = new ConfiguracionCrmApi
            {
                UrlServicio = "http://test.com/",
                key = "30462a9d27c6451e82eab03dcc422ca9",
                sessionId = "dfab9676-ac6f-4aa7-8ae5-3cff8a4747b1",
                instanciaEjecutarAccion = "ConectorCrm_MatrizUat",
                OAuthClientId = "cb9837d9-00a2-4b5e-ba5e-59b4848e6cec",
                OAuthClientSecret = "ger7Q~H3rRWiw5BblmSeL7Pmz.wqPPF4VIYtF",
                OAuthScope = "api://f7e527db-512d-4a72-b261-7d6220ca9da5/.default"
            };

            _configurationApiCrm = Options.Create(crmApi);

            var _inMemoryCollection = new Dictionary<string, string> {
                { "AccionConsultaPqrs", "epm_CCD_ConsultasPqrs" },
                { "AuthorizationBase:BaseApi", "http://test.com/" },
                { "AuthorizationBase:GrantType", "client_credentials" },
                { "AuthorizationApiCRM:ClientId", "cb9837d9-00a2-4b5e-ba5e-59b4848e6cec" },
                { "AuthorizationApiCRM:ClientSecret", "ger7Q~H3rRWiw5BblmSeL7Pmz.wqPPF4VIYtF" },
                { "AuthorizationApiCRM:Scope", "api://f7e527db-512d-4a72-b261-7d6220ca9da5/.default" },
            };

            _configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(_inMemoryCollection)
                .Build();
        }
    }
}

﻿using Epm.Crm.Cc.Plugin.ConsultaPqrs.Entidad;
using Epm.Crm.Cc.Plugin.ConsultaPqrs.Plugin;
using Ig.Xrm.Utilidades;
using Ig.Xrm.Utilidades.Conector;
using Ig.Xrm.Utilidades.Consumo;
using Ig.Xrm.Utilidades.Entidad;
using Ig.Xrm.Utilidades.Negocio;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Epm.Crm.Ccd.Plugin.ConsultaPqrsTest
{
    [TestClass]
    public class ConsultarPqrsTest
    {
        private readonly bool PruebaConectada = false;

        #region Propiedades
        private IPluginExecutionContext PluginExecutionContext;
        private IOrganizationServiceFactory Factory;
        private IOrganizationService service;
        private IServiceProvider ServiceProvider;
        private ITracingService TracingService;
        private IUtilidadXrm Xrm;
        #endregion

        #region Instanciación Dependencias
        [TestInitialize]
        public void TestInitialize()
        {
            if (PruebaConectada)
            {
                TestConectado();
            }
            else
            {
                TestNoConectado();
            }

            MockFactory();
            MockITracingService();
            MockIServiceProvider();
        }

        private void TestNoConectado()
        {
            MockUtilidadXrm();

            MockPluginExecutionContext(Guid.NewGuid());
        }

        private void TestConectado()
        {
            string url = "https://epm-vwt72.corp.epm.com.co/EPMCRMMatrizUAT/XRMServices/2011/Organization.svc";
            string dominio = "EPM";
            string usuario = "EPMCRMWFUAT";
            string password = "+3f3ct01nv3rn4d3r0*";

            AutenticacionCrm autenticacion = new AutenticacionCrm(url, dominio, usuario, password, true);
            Xrm = new UtilidadXrm(autenticacion);

            Guid organizationId = ((WhoAmIResponse)Xrm.ConsumoCrm.ServicioCRM.Execute(new WhoAmIRequest())).OrganizationId;
            MockPluginExecutionContext(organizationId);
        }

        #endregion Instanciación Dependencias

        #region Mocks Inicializador
        private void MockFactory()
        {
            Factory = MockRepository.GenerateStub<IOrganizationServiceFactory>();
            Factory.Stub(x => x.CreateOrganizationService(null)).IgnoreArguments().Return(Xrm.ConsumoCrm.ServicioCRM);
        }
        private void MockIServiceProvider()
        {
            ServiceProvider = MockRepository.GenerateStub<IServiceProvider>();
            ServiceProvider.Stub(x => x.GetService(null)).IgnoreArguments().Do((Func<Type, object>)delegate (Type t)
            {
                if (t == typeof(IPluginExecutionContext))
                {
                    return PluginExecutionContext;
                }
                else if (t == typeof(ITracingService))
                {
                    return TracingService;
                }
                else if (t == typeof(IOrganizationServiceFactory))
                {
                    return Factory;
                }

                return null;
            });
        }
        private void MockITracingService()
        {
            TracingService = MockRepository.GenerateStub<ITracingService>();
            TracingService.Stub(x => x.Trace(null, null)).IgnoreArguments().Do((Action<string, object[]>)delegate (string f, object[] o)
            {
                Debug.WriteLine(f, o);
            });
        }
        private void MockPluginExecutionContext(Guid organizationId)
        {
            PluginExecutionContext = MockRepository.GenerateStub<IPluginExecutionContext>();
            PluginExecutionContext.Stub(x => x.OrganizationId).IgnoreArguments().Return(organizationId);
            PluginExecutionContext.Stub(x => x.OutputParameters).IgnoreArguments().Return(new ParameterCollection());
            PluginExecutionContext.Stub(x => x.SharedVariables).IgnoreArguments().Return(new ParameterCollection());
            PluginExecutionContext.Stub(x => x.PreEntityImages).IgnoreArguments().Return(new EntityImageCollection());
        }
        private void MockUtilidadXrm()
        {

            Xrm = MockRepository.GenerateStub<IUtilidadXrm>();
            IConsumoCrm consumo = MockRepository.GenerateMock<IConsumoCrm>();
            IConectorCrm conector = MockRepository.GenerateMock<IConectorCrm>();
            IFecha fecha = MockRepository.GenerateMock<IFecha>();
            IUtilidad utilidad = MockRepository.GenerateMock<IUtilidad>();
            ILog log = MockRepository.GenerateMock<ILog>();

            service = MockRepository.GenerateMock<IOrganizationService>();

            Xrm.Stub(x => x.ConsumoCrm).IgnoreArguments().Return(consumo);

            Xrm.Stub(x => x.Conector).IgnoreArguments().Return(conector);

            Xrm.Stub(x => x.Fecha).IgnoreArguments().Return(fecha);

            Xrm.Stub(x => x.Utilidad).IgnoreArguments().Return(utilidad);

            Xrm.Stub(x => x.Log).IgnoreArguments().Return(log);

            Xrm.ConsumoCrm.Stub(x => x.ServicioCRM).Return(service);
        }
        #endregion Mocks Inicializador

        #region Mocks Datos
        private void MockDatosPqrs()
        {
            service.Stub(x => x.RetrieveMultiple(null)).IgnoreArguments().Return(
                new EntityCollection(new List<Entity>() { new Entity("epm_pypcausa")
                {
                    Attributes = {
                      { "incidentid", Guid.NewGuid()},
                      { "epm_pypcausaid", "" },
                      { "createdon", "" },
                      { "epm_pqroffice", "" },
                      { "epm_pqrfiledate", "" },
                      { "epm_pqrcause", "" },
                      { "ticketnumber", "" },
                      { "epm_pqrdeadlineresponse", "" },
                      { "description", "" },
                      { "customerid", "" },
                    }
                }
                })).Repeat.Once();
        }
        private string ConstruirObjetoPeticion()
        {
            DatosEntrada datosEntrada = new DatosEntrada()
            {
                NumeroIdentificacion = "3211114655",
                TipoIdentificacion = "10506575"
            };
            return JsonConvert.SerializeObject(datosEntrada);
        }
        #endregion Mocks Datos

        [TestMethod]
        public void ConsultarPqrs() {
            ConsultaPqrs accionDelPlugin = new ConsultaPqrs()
            {
                UtilidadXrm = Xrm
            };
            ParameterCollection parametro = new ParameterCollection();
            parametro.Add("JsonPeticion", (ConstruirObjetoPeticion()));

            PluginExecutionContext.Stub(x => x.InputParameters).Return(parametro);
            PluginExecutionContext.Stub(x => x.MessageName).IgnoreArguments().Return("epm_CCD_ConsultarPqrs");

            MockDatosPqrs();
            accionDelPlugin.Execute(ServiceProvider);
            Assert.IsNotNull(accionDelPlugin);
        }




    }
}

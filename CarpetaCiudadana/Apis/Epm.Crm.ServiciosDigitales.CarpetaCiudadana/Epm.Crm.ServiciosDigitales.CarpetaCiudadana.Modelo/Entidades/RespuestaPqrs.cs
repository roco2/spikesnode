﻿using System.Collections.Generic;

namespace Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Entidades
{
    public class RespuestaPqrs
    {
        public List<Pqrs> Respuesta { get; set; }
        public string Error { get; set; }
    }
}

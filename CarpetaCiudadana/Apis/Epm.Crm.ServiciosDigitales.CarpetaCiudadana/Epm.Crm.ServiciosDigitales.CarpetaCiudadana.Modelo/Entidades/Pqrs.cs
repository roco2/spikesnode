﻿namespace Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Entidades
{
    public class Pqrs
    {
        public string NumeroCaso { get; set; }
        public string FechaCreacion { get; set; }
        public string EstadoCaso { get; set; }
        public string TipoCaso { get; set; }
        public string Oficina { get; set; }
        public string FechaRadicado { get; set; }
        public string Causa { get; set; }
        public string Motivo { get; set; }
        public string FechaLimiteRespuesta { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Esquemas
{
    public class ContactoEsquema
    {
        public const string NombreEsquemaEntidad = "contact";
        public const string ContactoId = "contactid";
        public const string TipoIdentificacion = "epm_cltypeofidentification";
        public const string NumeroIdentificacion = "epm_clidentificationnumber";
        public const string NombreCompleto = "fullname";
        public const string Estado = "statecode";

        public static string[] ColumnasRetrive => new string[] { ContactoId, TipoIdentificacion, NumeroIdentificacion, NombreCompleto, Estado };
    }
}

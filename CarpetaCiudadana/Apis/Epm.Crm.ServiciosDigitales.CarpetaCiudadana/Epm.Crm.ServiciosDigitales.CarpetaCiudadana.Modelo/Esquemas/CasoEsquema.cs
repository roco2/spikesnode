﻿namespace Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Esquemas
{
    public class CasoEsquema
    {
        public const string NombreEsquemaEntidad = "incident";
        public const string CasoId = "incidentid";
        public const string EstadoCaso = "statuscode";
        public const string FechaCreacion = "createdon";
        public const string TipoCaso = "epm_pqrcasetypecode";
        public const string Oficina = "epm_pqroffice";
        public const string FechaRadicado = "epm_pqrfiledate";
        public const string Causa = "epm_pqrcause";
        public const string NumeroCaso = "ticketnumber";
        public const string FechaLimiteRespuesta = "epm_pqrdeadlineresponse";
        public const string Motivo = "description";
        public const string ContactoId = "customerid";
        public const string CuentaId = "epm_pqraccount";

        public static string[] ColumnasRetrive => new string[] { EstadoCaso, FechaCreacion, TipoCaso, Oficina, FechaRadicado, Causa, NumeroCaso, FechaLimiteRespuesta, Motivo, ContactoId };
    }
}

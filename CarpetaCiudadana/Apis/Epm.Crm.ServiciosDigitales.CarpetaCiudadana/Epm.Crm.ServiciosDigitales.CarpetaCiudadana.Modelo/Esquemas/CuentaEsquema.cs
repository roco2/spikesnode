﻿namespace Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Esquemas
{
    public class CuentaEsquema
    {
        public const string NombreEsquemaEntidad = "account";
        public const string CuentaId = "accountid";
        public const string TipoIdentificacion = "epm_cljtypeofidentification";
        public const string NumeroIdentificacion = "accountnumber";

        public static string[] ColumnasRetrive => new string[] { CuentaId, TipoIdentificacion, NumeroIdentificacion};
    }
}

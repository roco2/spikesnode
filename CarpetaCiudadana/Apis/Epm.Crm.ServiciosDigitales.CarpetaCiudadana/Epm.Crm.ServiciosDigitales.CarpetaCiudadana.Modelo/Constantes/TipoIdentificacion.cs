﻿namespace Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Constantes
{
    public class TipoIdentificacion
    {
        public const int Cedula = 919900000;
        public const int CedulaExtranjeria = 919900001;
        public const int Pasaporte = 919900002;
    }
}

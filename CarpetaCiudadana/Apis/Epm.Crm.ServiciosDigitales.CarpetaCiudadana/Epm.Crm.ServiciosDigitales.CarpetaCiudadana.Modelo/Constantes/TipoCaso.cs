﻿
namespace Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Constantes
{
    public class TipoCaso
    {
        public const int Peticion = 1;
        public const int Queja = 2;
        public const int Reclamo = 3;
        public const int Recurso = 4;
        public const int AccionesJudiciales = 5;
    }
}

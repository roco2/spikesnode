﻿namespace Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Constantes
{
    public class EstadoCaso
    {
        public const int Abierto = 1;
        public const int TareasFinalizadas = 3;
        public const int TareasPendientes = 2;
        public const int PendienteEnvio = 919900004;
        public const int PendienteFallo = 919900000;
        public const int PendienteNotificacion = 4;
        public const int PendienteRecurso = 919900008;
        public const int Cerrado = 5;
    }
}

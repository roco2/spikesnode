﻿using Epm.Crm.Cc.Plugin.ConsultaPqrs.Controlador;
using Epm.Crm.Cc.Plugin.ConsultaPqrs.Entidad;
using Epm.Crm.Cc.Plugin.ConsultaPqrs.Interface;
using Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Entidades;
using Ig.Xrm.Utilidades;
using Ig.Xrm.Utilidades.Entidad;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Epm.Crm.Cc.Plugin.ConsultaPqrs.Plugin
{
    public sealed class ConsultaPqrs: IPlugin
    {
        private readonly string NombreAccionConsulta = "epm_CCD_ConsultasPqrs";
        public IUtilidadXrm UtilidadXrm { get; set; }

        public void Execute(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException(nameof(serviceProvider), "El valor no puede ser nulo");
            }

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            IUtilidadXrm Xrm = UtilidadXrm ?? new UtilidadXrm(service);
            RespuestaPqrs respuesta = new RespuestaPqrs();

            try
            {
                if (context.MessageName == NombreAccionConsulta)
                {
                    string jsonPeticion = context?.InputParameters["JsonPeticion"] != null ? context?.InputParameters["JsonPeticion"].ToString() : string.Empty;

                    if (!string.IsNullOrEmpty(jsonPeticion))
                    {
                        IControladorConsultaPqrs controlador = new ControladorConsultaPqrs(Xrm);
                        respuesta = controlador.ConsultarPqrs(jsonPeticion);
                    }
                }
            }
            catch (InvalidPluginExecutionException ex)
            {

                Xrm.Log.RegistrarLog(
                new DatosLog("CarpetaCiudadana",
                $"Ocurrió un error en el plugin: {MethodBase.GetCurrentMethod().DeclaringType.Name}",
                TipoError.Excepcion,
                TipoOperacion.Crm,
                ex,
                null,
                "Carpeta Ciudadana Consulta Pqrs"));
            }
            catch (Exception ex)
            {

                Xrm.Log.RegistrarLog(
                new DatosLog("CarpetaCiudadana",
                $"Ocurrió un error en el plugin: {MethodBase.GetCurrentMethod().DeclaringType.Name}",
                TipoError.Excepcion,
                TipoOperacion.Crm,
                ex,
                null,
                "Carpeta Ciudadana Consulta Pqrs"));

                throw new InvalidPluginExecutionException(ex.Message, ex);
            }
            context.OutputParameters["JsonRespuesta"] = JsonConvert.SerializeObject(respuesta);
        }
    }
}

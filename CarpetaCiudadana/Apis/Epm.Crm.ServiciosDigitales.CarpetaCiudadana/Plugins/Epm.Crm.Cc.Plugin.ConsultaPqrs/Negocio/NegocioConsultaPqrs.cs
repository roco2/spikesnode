﻿using Epm.Crm.Cc.Plugin.ConsultaPqrs.Entidad;
using Epm.Crm.Cc.Plugin.ConsultaPqrs.Interface;
using Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Constantes;
using Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Entidades;
using Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Esquemas;
using Ig.Xrm.Utilidades;
using Ig.Xrm.Utilidades.Entidad;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Epm.Crm.Cc.Plugin.ConsultaPqrs.Negocio
{
    public class NegocioConsultaPqrs: INegocioConsultaPqrs
    {
        private IUtilidadXrm Xrm { get; set; }
        private readonly int UltimosMesesConsulta = 6;

        public NegocioConsultaPqrs(IUtilidadXrm xrm)
        {
            Xrm = xrm;
        }
        public RespuestaPqrs ConsultarDatosPqrs(string JsonPeticion)
        {
            DatosEntrada peticion = ResolverJsonPeticion(JsonPeticion);
            if (peticion != null && peticion.NumeroIdentificacion != null && peticion.TipoIdentificacion != null)
            {
                QueryExpression query = new QueryExpression(CasoEsquema.NombreEsquemaEntidad);
                query.ColumnSet = new ColumnSet(CasoEsquema.ColumnasRetrive);
                int tipoIdentificacion = MapearTipoIdentificacion(peticion.TipoIdentificacion);
                if (tipoIdentificacion == 0)
                {
                    RespuestaPqrs respuestaError = new RespuestaPqrs();
                    respuestaError.Error = "El tipo de identificación no es válido.";
                    return respuestaError;
                }
                LinkEntity joinRelacion;
                joinRelacion = JoinRelacionContacto(peticion.NumeroIdentificacion, tipoIdentificacion);

                query.LinkEntities.Add(joinRelacion);
                query.Criteria = AgregarCriteriosConsulta();
                EntityCollection respuesta = Xrm.ConsumoCrm.ServicioCRM.RetrieveMultiple(query);

                return LlenarDatos(respuesta?.Entities?.ToList());
            }
            else
            {
                string Error = "";
                if (peticion.NumeroIdentificacion == null)
                {
                    Error = "No se ha encontrado un valor para el campo NumeroIdentificacion.";
                }
                if (peticion.TipoIdentificacion == null)
                {
                    Error = Error + "No se ha encontrado un valor para el campo TipoIdentificacion.";
                }
                RespuestaPqrs respuesta = new RespuestaPqrs();
                respuesta.Error = Error;
                return respuesta;
            }
        }

        private DatosEntrada ResolverJsonPeticion(string jsonPeticion)
        {
            DatosEntrada datosentrada = null;
            if (!string.IsNullOrEmpty(jsonPeticion))
            {
                try
                {
                    datosentrada = JsonConvert.DeserializeObject<DatosEntrada>(jsonPeticion);
                }
                catch (Exception ex)
                {
                    Xrm.Log.RegistrarLog(
                    new DatosLog("CarpetaCiudadana",
                    $"No fue posible deserializar el objeto para obtener la información: {jsonPeticion}",
                    TipoError.Excepcion,
                    TipoOperacion.Crm,
                    ex,
                    null,
                    "Carpeta Ciudadana Consulta Pqrs"));

                    throw new InvalidPluginExecutionException(ex.Message, ex);
                }
            }
            return datosentrada;
        }

        private FilterExpression AgregarCriteriosConsulta()
        {
            FilterExpression filterFecha = new FilterExpression(LogicalOperator.And);
            filterFecha.AddCondition(CasoEsquema.FechaCreacion, ConditionOperator.LastXMonths, UltimosMesesConsulta);

            FilterExpression filterEstado = new FilterExpression(LogicalOperator.Or);
            filterEstado.AddCondition(CasoEsquema.EstadoCaso, ConditionOperator.Equal, EstadoCaso.Abierto);
            filterEstado.AddCondition(CasoEsquema.EstadoCaso, ConditionOperator.Equal, EstadoCaso.TareasFinalizadas);
            filterEstado.AddCondition(CasoEsquema.EstadoCaso, ConditionOperator.Equal, EstadoCaso.TareasPendientes);
            filterEstado.AddCondition(CasoEsquema.EstadoCaso, ConditionOperator.Equal, EstadoCaso.PendienteEnvio);
            filterEstado.AddCondition(CasoEsquema.EstadoCaso, ConditionOperator.Equal, EstadoCaso.PendienteFallo);
            filterEstado.AddCondition(CasoEsquema.EstadoCaso, ConditionOperator.Equal, EstadoCaso.PendienteNotificacion);
            filterEstado.AddCondition(CasoEsquema.EstadoCaso, ConditionOperator.Equal, EstadoCaso.PendienteRecurso);
            filterEstado.AddCondition(CasoEsquema.EstadoCaso, ConditionOperator.Equal, EstadoCaso.Cerrado);

            filterFecha.AddFilter(filterEstado);

            return filterFecha;
        }

        private LinkEntity JoinRelacionContacto(string numeroIdentificacion, int tipoIdentificacion)
        {
            LinkEntity relacionContacto = new LinkEntity(CasoEsquema.NombreEsquemaEntidad,
                                                ContactoEsquema.NombreEsquemaEntidad,
                                                CasoEsquema.ContactoId,
                                                ContactoEsquema.ContactoId,
                                                JoinOperator.Inner);
            relacionContacto.EntityAlias = "Contacto";
            relacionContacto.LinkCriteria.AddCondition(ContactoEsquema.NumeroIdentificacion, ConditionOperator.Equal, numeroIdentificacion);
            relacionContacto.LinkCriteria.AddCondition(ContactoEsquema.TipoIdentificacion, ConditionOperator.Equal, tipoIdentificacion);

            return relacionContacto;
        }

        private RespuestaPqrs LlenarDatos(List<Entity> datosOficinas)
        {
            RespuestaPqrs respuesta = new RespuestaPqrs();
            List<Pqrs> ListaDatosPqrs = new List<Pqrs>();
            foreach (var item in datosOficinas)
            {
                Pqrs pqrs = new Pqrs();
                if (item.Contains(CasoEsquema.NumeroCaso))
                {
                    pqrs.NumeroCaso = item.Attributes[CasoEsquema.NumeroCaso].ToString();
                }
                if (item.Contains(CasoEsquema.FechaCreacion))
                {
                    DateTime fechaCreacion = DateTime.Parse(item.Attributes[CasoEsquema.FechaCreacion].ToString());
                    pqrs.FechaCreacion = fechaCreacion.ToLocalTime().ToString("yyyy'-'MM'-'dd HH':'mm':'ss.ff");
                }
                if (item.Contains(CasoEsquema.FechaLimiteRespuesta))
                {
                    DateTime fechaLimite = DateTime.Parse(item.Attributes[CasoEsquema.FechaLimiteRespuesta].ToString());
                    pqrs.FechaLimiteRespuesta = fechaLimite.ToLocalTime().ToString("yyyy'-'MM'-'dd HH':'mm':'ss.ff");
                }
                if (item.Contains(CasoEsquema.Motivo))
                {
                    pqrs.Motivo = item.Attributes[CasoEsquema.Motivo].ToString();
                }
                if (item.Contains(CasoEsquema.FechaRadicado))
                {
                    DateTime fechaRadicado = DateTime.Parse(item.Attributes[CasoEsquema.FechaRadicado].ToString());
                    pqrs.FechaRadicado = fechaRadicado.ToLocalTime().ToString("yyyy'-'MM'-'dd HH':'mm':'ss.ff");
                }
                if (item.Contains(CasoEsquema.Oficina))
                {
                    EntityReference oficina = item.GetAttributeValue<EntityReference>(CasoEsquema.Oficina);
                    pqrs.Oficina = oficina.Name;
                }
                if (item.Contains(CasoEsquema.EstadoCaso))
                {
                    OptionSetValue estado = item.GetAttributeValue<OptionSetValue>(CasoEsquema.EstadoCaso);
                    pqrs.EstadoCaso = MapearEstadoCaso(estado.Value); 
                }
                if (item.Contains(CasoEsquema.TipoCaso))
                {
                    OptionSetValue tipoCaso = item.GetAttributeValue<OptionSetValue>(CasoEsquema.TipoCaso);
                    pqrs.TipoCaso = MapearTipoCaso(tipoCaso.Value);
                }
                if (item.Contains(CasoEsquema.Causa))
                {
                    EntityReference causa = item.GetAttributeValue<EntityReference>(CasoEsquema.Causa);
                    pqrs.Causa = causa.Name;
                }
                ListaDatosPqrs.Add(pqrs);
            }
            respuesta.Respuesta = ListaDatosPqrs;
            return respuesta;
        }

        private string MapearEstadoCaso(int tipoCasoId)
        {
            switch (tipoCasoId)
            {
                case EstadoCaso.Abierto:
                    return "En Trámite";
                case EstadoCaso.TareasFinalizadas:
                    return "En Trámite";
                case EstadoCaso.TareasPendientes:
                    return "En Trámite";
                case EstadoCaso.PendienteEnvio:
                    return "Pendiente envío SSPD";
                case EstadoCaso.PendienteFallo:
                    return "Pendiente Fallo SSPD";
                case EstadoCaso.PendienteNotificacion:
                    return "Pendiente Notificación";
                case EstadoCaso.PendienteRecurso:
                    return "Pendiente Recurso";
                case EstadoCaso.Cerrado:
                    return "Cerrado";
                default:
                    return "";
            }
        }

        private int MapearTipoIdentificacion(string tipoIdentificacion)
        {
            switch (tipoIdentificacion)
            {
                case "919900000":
                    return TipoIdentificacion.Cedula;
                case "919900001":
                    return TipoIdentificacion.CedulaExtranjeria;
                case "919900002":
                    return TipoIdentificacion.Pasaporte;
                default:
                    return 0;
            }
        }
        private string MapearTipoCaso(int tipoCasoId)
        {
            switch (tipoCasoId)
            {
                case TipoCaso.AccionesJudiciales:
                    return "Acciones Judiciales";
                case TipoCaso.Peticion:
                    return "Petición";
                case TipoCaso.Queja:
                    return "Queja";
                case TipoCaso.Reclamo:
                    return "Reclamo";
                case TipoCaso.Recurso:
                    return "Recurso";
                default:
                    return "";
            }
        }
    }
}

﻿using Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Entidades;
namespace Epm.Crm.Cc.Plugin.ConsultaPqrs.Interface
{
    public interface INegocioConsultaPqrs
    {
        RespuestaPqrs ConsultarDatosPqrs(string JsonPeticion);
    }
}

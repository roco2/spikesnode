﻿using Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Entidades;
namespace Epm.Crm.Cc.Plugin.ConsultaPqrs.Interface
{
    public interface IControladorConsultaPqrs
    {
        RespuestaPqrs ConsultarPqrs(string JsonPeticion);
    }
}

﻿using Epm.Crm.Cc.Plugin.ConsultaPqrs.Interface;
using Epm.Crm.Cc.Plugin.ConsultaPqrs.Negocio;
using Epm.Crm.ServiciosDigitales.CarpetaCiudadana.Modelo.Entidades;
using Ig.Xrm.Utilidades;
using System.Collections.Generic;

namespace Epm.Crm.Cc.Plugin.ConsultaPqrs.Controlador
{
    public class ControladorConsultaPqrs: IControladorConsultaPqrs
    {
        public IUtilidadXrm UtilidadXrm { get; set; }

        public ControladorConsultaPqrs(IUtilidadXrm utilidadXrm)
        {
            UtilidadXrm = utilidadXrm;
        }

        public RespuestaPqrs ConsultarPqrs(string JsonPeticion)
        {
            INegocioConsultaPqrs consultaPqrs = new NegocioConsultaPqrs(UtilidadXrm);
            RespuestaPqrs listaPqrs = consultaPqrs.ConsultarDatosPqrs(JsonPeticion);
            return listaPqrs;
        }
    }
}

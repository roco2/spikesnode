import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-component',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public fileToUpload: any;
  private chunkSize = 1024 * 1024 * 10; // 10MB
  private totalChunks: number = 0;
  private chunkIndex = 0;
  private fileUploadToken: string = '';
  private headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  onFileSelected(event: any) {
    if(event.target.files && event.target.files.length) {
      this.fileToUpload = event.target.files[0];
    }
    return;
  }

  uploadFile() {
    this.totalChunks = Math.ceil(this.fileToUpload.size / this.chunkSize);
    this.chunkIndex = 0;
    this.fileUploadToken = '';

    // Send metadata to server to obtain a token for the file upload
    const metadata = { fileName: this.fileToUpload.name, fileSize: this.fileToUpload.size, chunkSize: this.chunkSize };
    this.http.post('/api/FileUpload/Initialize', metadata, { headers: this.headers }).subscribe((response: any) => {
      this.fileUploadToken = response.fileUploadToken;
      this.uploadChunk();
    });
  }

  uploadChunk() {
    const start = this.chunkIndex * this.chunkSize;
    const end = Math.min(this.fileToUpload.size, start + this.chunkSize);
    const chunk = this.fileToUpload.slice(start, end);

    // Send chunk to server with the token received from the Initialize request
    this.http.post('/api/FileUpload/UploadChunk', chunk, { headers: this.headers, params: { fileUploadToken: this.fileUploadToken, chunkIndex: this.chunkIndex.toString() } }).subscribe((response: any) => {
      this.chunkIndex++;
      if (this.chunkIndex < this.totalChunks) {
        this.uploadChunk();
      } else {
        // Send complete request to the server to merge the uploaded chunks into a single file
        this.http.post('/api/FileUpload/Complete', null, { headers: this.headers, params: { fileUploadToken: this.fileUploadToken } }).subscribe((response: any) => {
          console.log('Upload completed!');
        });
      }
    });
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FileComponent } from './file/file.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
      FileComponent
   ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [FileComponent]
})
export class AppModule { }

import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent {
  fileToUpload: any = null;

  constructor(private http: HttpClient) { }

  onFileSelected(file: any) {
    this.fileToUpload = file.files[0];
  }

  upload() {
    if (!this.fileToUpload) {
      console.log('Selecciona un archivo primero');
      return;
    }

    const chunkSize = 1073741824 / 5; // Tamaño de chunk en bytes (500 MB)
    const fileSize = this.fileToUpload.size;
    const chunks = Math.ceil(fileSize / chunkSize);

    console.log('Comenzando subida de archivo...');
    console.log('Tamaño total del archivo: ' + fileSize + ' bytes');
    console.log('Tamaño de cada chunk: ' + chunkSize + ' bytes');
    console.log('Número total de chunks: ' + chunks);

    let currentChunk = 0;

    const uploadFileChunk = (file: File, start: number, end: number) => {

      console.log('Subiendo chunk ' + (currentChunk + 1) + ' de ' + chunks);
      const formData = new FormData();
      const chunkName = file.name + '-' + currentChunk;
      formData.append('file', file.slice(start, end), chunkName);

      formData.append('index', currentChunk.toString());
      formData.append('totalChunks', chunks.toString());
      this.http.post<any>('https://localhost:44375/upload', formData, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data'
        })
      }).subscribe(
        response => {
          console.log(response.message);
          currentChunk++;
          if (currentChunk < chunks) {
            uploadFileChunk(file, currentChunk * chunkSize, (currentChunk + 1) * chunkSize);
          } else {
            console.log('Todos los chunks se han subido correctamente');
            this.http.get<any>('https://localhost:44375/merge?fileName=' + file.name).subscribe(
              response => {
                console.log(response.message);
              },
              error => {
                console.log(error);
              }
            );
          }
        },
        error => {
          console.log(error);
        }
      );
    };

    uploadFileChunk(this.fileToUpload, 0, chunkSize);
  }
}

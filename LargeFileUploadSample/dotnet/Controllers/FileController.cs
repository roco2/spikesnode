﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;

namespace LargeFileUpload.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private const int MaxFileSize = 10 * 1024 * 1024; // 10 MB
        private const string UploadDirectory = "./Uploads/";

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(IFormFile file, string fileName, int totalChunks, int chunkIndex)
        {
            if (file.Length > MaxFileSize)
            {
                return BadRequest($"The maximum allowed file size is {MaxFileSize / 1024 / 1024} MB.");
            }


            if (!Directory.Exists(UploadDirectory))
            {
                Directory.CreateDirectory(UploadDirectory);
            }

            var filePath = Path.Combine(UploadDirectory, fileName);

            using (var stream = new FileStream(filePath, chunkIndex == 0 ? FileMode.Create : FileMode.Append))
            {
                await file.CopyToAsync(stream);
            }

            if (chunkIndex == totalChunks - 1)
            {
                // All chunks have been uploaded
                // Do something with the completed file
                // For example, save it to a database or process it
            }

            return Ok();
        }
    }

}



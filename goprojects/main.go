package main

import (
	"fmt"
)

func main() {

	//Variables
	var myString string = "El valor del digito es:"
	var myNumbers int = 12
	var myBool bool = true
	var myArray [3]int
	myArray[0] = 6
	//Similar Dictionary en c#
	myMap := make(map[string]int)
	myMap["Key"] = 0
	myMap["Roco"] = 1
	//Condicionales
	if myBool {
		myNumbers += 1
	} else {
		myNumbers -= 1
	}

	// Estructuras simil a Clases
	type MyStruct struct {
		name string
		age  int
	}
	myStructObj := MyStruct{"Roco", 35}
	//Metodos librerias nativas

	//fmt.Println(myString, reflect.TypeOf(myNumbers))
	fmt.Println(myString, myNumbers)
	fmt.Println(myArray)
	fmt.Println(myMap["Roco"])
	fmt.Println(myStructObj)

}

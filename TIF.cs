﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using System.Runtime.InteropServices;
using Common.Util;
using WebTiffProcessor.Properties;
using WebTiffProcessor.Clases;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Xml;
using ImageMagick;
using Support.Traceability.DTO;
using Support.Traceability.Impl;



namespace Presentation.Pages.ConsultaCarpetaUnica.Clases
{



    /// <summary>
    /// Creado por Carlos Alberto Rivera Múnera
    /// 16 mayo 2014
    /// 
    /// Clase que contiene los metodos para el manejo de TIF
    /// </summary>
    public class TIF
    {
        // Fields
        private bool m_Disposed { get; set; }
        private Image m_Img = null;
        private int m_PageCount = -1;
        public String m_FilePathName;
        public String fileID { get; set; }
        public String User { get; set; }
        public bool ValidaPerfil { get; set; }
        public static String ReadOnly = "";
        public static long concurrent = 0;
        public bool RequiereMiniaturas = true;
        SystemParameterDTO CalidadImagenDTO = TraceabilityMngrFacade.GetInstance().GetSystemParametersByDescription(Settings.Default.CalidadImagen);

        #region CONSTRUCTOR/DESTRUCTOR

        /// <summary>
        /// Creates an instance of this class
        /// </summary>
        /// <param name="FilePathName"></param>
        public TIF(String FilePathName, String FolderFile = "")
        {
            ValidaPerfil = true;
            if (FilePathName == null) throw new Exception("FilePathName cannot be NULL");
            if (FilePathName == "") throw new Exception("FilePathName cannot be NULL");
            m_FilePathName = FilePathName;
            fileID = FolderFile;
            this.calidad = Convert.ToInt32(CalidadImagenDTO.Valor);
        }

        public TIF()
        {
            this.calidad = Convert.ToInt32(CalidadImagenDTO.Valor);
        }

        /// <summary>
        /// Disposes of any resources still available
        /// </summary>
        public void Dispose()
        {
            if (this.m_Img != null)
            {
                this.m_Img.Dispose();
            }
            this.m_Disposed = true;
        }


        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets the Page Count of the TIF
        /// </summary>
        public int PageCount
        {
            get
            {
                try
                {
                    if (m_PageCount == -1)
                        this.m_PageCount = GetPageCount();

                    return m_PageCount;
                }
                catch (Exception exc)
                {
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "No se pudo obtener información de la imágen: " + exc.Message);
                    throw new Exception("No se pudo procesar la imagen, inténtelo más tarde. archivo: " + this.m_FilePathName);
                }
            }
        }


        #endregion

        #region METHODS

        /// <summary>
        /// Returns the page count of the TIF
        /// </summary>
        /// <returns></returns>
        private int GetPageCount()
        {
            int Pgs = -1;
            Image Img = null;
            try
            {
                Img = Image.FromFile(this.m_FilePathName);
                Pgs = Img.GetFrameCount(FrameDimension.Page);
                return Pgs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Img.Dispose();
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// Returns an Image of a TIF page
        /// </summary>
        /// <param name="PageNum"></param>
        /// <returns></returns>
        public TIFF_Image GetTiffImage(int PageNum)
        {


            if ((PageNum < 1) | (PageNum > this.PageCount))
            {
                throw new InvalidOperationException("Page to be retrieved is outside the bounds of the total TIF file pages.  Please choose a page number that exists.");
            }

            String DirectorioCalidad;

            if (!EsBajaCalidad())
            {
                DirectorioCalidad = "AltaCalidad";
            }
            else
            {
                DirectorioCalidad = "BajaCalidad";
            }


            //Parametros de ruta del Archivo
            String ruta_archivo_temp = this.m_FilePathName;
            FileInfo file = new FileInfo(ruta_archivo_temp);
            String Archivo = file.Name.Split('.')[0];
            String Directorio = Path.GetTempPath() + Settings.Default.UbicacionImgCarpetaUnicaTEMP + "\\" + Archivo + "\\" + DirectorioCalidad;

            String DirectorioConArchivo = Directorio + "\\" + Archivo;

            //si el directorio no existe se crea
            if (!Directory.Exists(Directorio))
            {

                Directory.CreateDirectory(Directorio);
                CreateTrace.WriteLog(CreateTrace.LogLevel.Error, "Crea directorio: " + Directorio);
                CrearArchivosPaginas(calidad);
            }


            if ((new DirectoryInfo(Directorio)).GetFiles().Length > (RequiereMiniaturas ? 2 : 1))
            {
                String rutaArchivo = DirectorioConArchivo + "-" + (PageNum - 1) + (EsBajaCalidad() ? ".png" : ".jpeg");
                return new TIFF_Image() { Imagen = System.Drawing.Image.FromFile(rutaArchivo), RutaImagen = rutaArchivo, Formato = (EsBajaCalidad() ? ImageFormat.Png : ImageFormat.Jpeg) };
            }
            else
            {
                String rutaArchivo = DirectorioConArchivo + (EsBajaCalidad() ? ".png" : ".jpeg");
                return new TIFF_Image() { Imagen = System.Drawing.Image.FromFile(rutaArchivo), RutaImagen = rutaArchivo, Formato = (EsBajaCalidad() ? ImageFormat.Png : ImageFormat.Jpeg) };

            }

        }
        /// <summary>
        /// Retorna informacion de la pagina TIF, metodo utilizado para impresión de tapas.
        /// </summary>
        /// <param name="PageNum"></param>
        /// <returns></returns>
        public TIFF_Image GetInfImage(int PageNum)
        {


            if ((PageNum < 1) | (PageNum > this.PageCount))
            {
                throw new InvalidOperationException("Page to be retrieved is outside the bounds of the total TIF file pages.  Please choose a page number that exists.");
            }

            //Parametros de ruta del Archivo
            String ruta_archivo_temp = this.m_FilePathName;
            FileInfo file = new FileInfo(ruta_archivo_temp);
            String Archivo = file.Name.Split('.')[0];
            String Directorio = Path.GetTempPath() + Settings.Default.UbicacionImgCarpetaUnicaTEMP + "\\" + Archivo;

            String DirectorioConArchivo = Directorio + "\\" + Archivo;

            //si el directorio no existe se crea
            if (!Directory.Exists(Directorio))
            {

                Directory.CreateDirectory(Directorio);
                CreateTrace.WriteLog(CreateTrace.LogLevel.Error, "Crea directorio: " + Directorio);
                CrearArchivosPaginasTapas();
            }


            if ((new DirectoryInfo(Directorio)).GetFiles().Length > (RequiereMiniaturas ? 2 : 1))
            {
                String rutaArchivo = DirectorioConArchivo + "-" + (PageNum - 1) + (EsBajaCalidad() ? ".png" : ".jpeg");
                return new TIFF_Image() { Imagen = System.Drawing.Image.FromFile(rutaArchivo), RutaImagen = rutaArchivo, Formato = (EsBajaCalidad() ? ImageFormat.Png : ImageFormat.Jpeg) };
            }
            else
            {
                String rutaArchivo = DirectorioConArchivo + (EsBajaCalidad() ? ".png" : ".jpeg");
                return new TIFF_Image() { Imagen = System.Drawing.Image.FromFile(rutaArchivo), RutaImagen = rutaArchivo, Formato = (EsBajaCalidad() ? ImageFormat.Png : ImageFormat.Jpeg) };

            }

        }

        /// <summary>
        /// Procesa las imágenes con Image Magick para las tapas de inventario
        /// </summary>
        /// <returns>Ubicación de las imágenes divididas</returns>
        public String CrearArchivosPaginasTapas()
        {


            try
            {
                //reinicia contador
                if (concurrent < 0)
                    concurrent = 0;



                //informacion del archivo
                String ruta_archivo_temp = this.m_FilePathName;
                FileInfo file = new FileInfo(ruta_archivo_temp);
                String Archivo = file.Name.Split('.')[0];
                String Directorio = Path.GetTempPath() + Settings.Default.UbicacionImgCarpetaUnicaTEMP + "\\" + Archivo;


                try
                {
                    //si el directorio no existe se crea
                    if (!Directory.Exists(Directorio))
                    {
                        Directory.CreateDirectory(Directorio);
                    }

                }
                catch (Exception ex)
                {

                    throw new Exception("No se pudo crear el directorio: " + Directorio + " Verifique los permisos de escritura");
                }

                //escribimos en el log el apuntamiento de ImageMagick
                CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Ruta Image Magick: " + Settings.Default.RutaConvertImageMagick);

                //verificamos que existea imageMagick
                if (!File.Exists(Settings.Default.RutaConvertImageMagick))
                    throw new Exception("Por favor contacte al administrador: Image Magick no se encuentra instalado o se encuentra mal configurado");


                //Obtener grandes
                using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                {

                    String Depth = "";
                    if (EsBajaCalidad())
                    {
                        Depth += "-depth 1 -colorspace GRAY ";
                    }
                    else
                    {
                        using (Image img = Image.FromFile(ruta_archivo_temp))
                        {

                            if (img.PixelFormat != PixelFormat.Format1bppIndexed)
                            {
                                //Depth += "-depth 8 -strip -interlace Plane -gaussian-blur 0.05 -quality 70% -colorspace GRAY";
                                if (ValidaPerfil)
                                    Depth += "-depth 8 -strip -interlace Plane -quality 70% -colorspace GRAY -resize 70%";
                            }
                            else
                            {
                                Depth += " -depth 1  -colorspace GRAY -resize 70%";
                            }
                        }
                    }

                    //espera en la cola determinado tiempo, mientras terminan los que se estan procesnado
                    lock (ReadOnly)
                    {

                        //entro
                        concurrent++;
                        //si actualmente el concurrente supero los maximos permitidos
                        if (concurrent > Settings.Default.MaximoConvertConcurrentes)
                        {


                            CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "esperando lugar para procesar: " + ", hora: " + System.DateTime.Now.ToLongDateString() + " " + System.DateTime.Now.ToLongTimeString());
                            Stopwatch timer = new Stopwatch();
                            timer.Start();
                            while (timer.Elapsed.TotalSeconds < Settings.Default.MaximoTiempoEsperaConvert)
                            {
                                if (concurrent <= Settings.Default.MaximoConvertConcurrentes)
                                {
                                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Entro a procesarse a las: " + ruta_archivo_temp + ", hora: " + System.DateTime.Now.ToLongDateString() + " " + System.DateTime.Now.ToLongTimeString());
                                    break;
                                }
                            }
                            timer.Stop();

                        }
                    }
                    //Se ejecuta el convert
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    startInfo.FileName = Settings.Default.RutaConvertImageMagick;

                    long MuestraMemoria = 0;
                    long cont = 0;
                    //se toma el tiempo inicial antes del convert
                    DateTime TiempoConverts = System.DateTime.Now;

                    //log procesando tiff
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Procesando tiff en la ubicacion: " + Directorio + "\\" + Archivo + (EsBajaCalidad() ? ".png" : ".jpeg"));
                    String ArgumentosConvert = "\"" + ruta_archivo_temp + "\" " + Depth + " \"" + Directorio + "\\" + Archivo + (EsBajaCalidad() ? ".png" : ".jpeg") + "\"";

                    //Argumentos convert en el log
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "convert " + ArgumentosConvert);
                    startInfo.Arguments = ArgumentosConvert;
                    process.StartInfo = startInfo;
                    process.Start();
                    process.WaitForExit();

                    //Obtener thumbs
                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    startInfo.FileName = Settings.Default.RutaConvertImageMagick;

                    if (RequiereMiniaturas)
                    {
                        ArgumentosConvert = "\"" + ruta_archivo_temp + "\" " + Depth + " -thumbnail 100x100 -compress JPEG \"" + Directorio + "\\" + Archivo + "_Tmb" + (EsBajaCalidad() ? ".png" : ".jpeg") + "\"";
                        //Argumentos convert en el log
                        CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "convert " + ArgumentosConvert);
                        startInfo.Arguments = ArgumentosConvert;
                        process.StartInfo = startInfo;

                        process.Start();

                        process.WaitForExit();
                        CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "**Memoria ImageMagick Miniaturas: " + SizeSuffix(MuestraMemoria));
                    }



                    process.Close();

                    //reciclar variables
                    //GC.Collect();
                    //GC.WaitForPendingFinalizers();

                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "**Tiempo ImageMagick(Inicio: " + TiempoConverts.ToLongTimeString() + "  Fin: " + System.DateTime.Now.ToLongTimeString() + "  ): " + (System.DateTime.Now - TiempoConverts).Seconds + "." + (System.DateTime.Now - TiempoConverts).Milliseconds + " Segundos");


                    return Directorio;

                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                concurrent--;
            }
        }
        /// <summary>
        /// Procesa las imágenes con Image Magick para visor imagenes
        /// </summary>
        /// <returns>Ubicación de las imágenes divididas</returns>
        public String CrearArchivosPaginas(int CalidadImagen)
        {

            this.calidad = CalidadImagen;
            try
            {
                //reinicia contador
                if (concurrent < 0)
                    concurrent = 0;

                String DirectorioCalidad;
                if (!EsBajaCalidad())
                {
                    DirectorioCalidad = "AltaCalidad";
                }
                else
                {
                    DirectorioCalidad = "BajaCalidad";
                }



                //informacion del archivo
                String ruta_archivo_temp = this.m_FilePathName;
                FileInfo file = new FileInfo(ruta_archivo_temp);
                String Archivo = file.Name.Split('.')[0];
                String Directorio = Path.GetTempPath() + Settings.Default.UbicacionImgCarpetaUnicaTEMP + Path.DirectorySeparatorChar + Archivo + Path.DirectorySeparatorChar + DirectorioCalidad;
                String DirectorioBase = Path.GetTempPath() + Settings.Default.UbicacionImgCarpetaUnicaTEMP + Path.DirectorySeparatorChar + Archivo;


                try
                {
                    //si el directorio no existe se crea
                    if (!Directory.Exists(Directorio))
                    {
                        Directory.CreateDirectory(Directorio);
                    }

                }
                catch (Exception ex)
                {

                    throw new Exception("No se pudo crear el directorio: " + Directorio + " Verifique los permisos de escritura");
                }

                //escribimos en el log el apuntamiento de ImageMagick
                //CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Ruta Image Magick: " + Settings.Default.RutaConvertImageMagick);

                //verificamos que existea imageMagick
                //if (!File.Exists(Settings.Default.RutaConvertImageMagick))
                //    throw new Exception("Por favor contacte al administrador: Image Magick no se encuentra instalado o se encuentra mal configurado");


                //Obtener grandes
                using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                {

                    String Depth = "";
                    if (CalidadImagen == 2)
                    {
                        Depth += "-depth 1 -colorspace GRAY ";
                    }
                    else
                    {
                        using (Image img = Image.FromFile(ruta_archivo_temp))
                        {

                            if (img.PixelFormat != PixelFormat.Format1bppIndexed)
                            {
                                //Depth += "-depth 8 -strip -interlace Plane -gaussian-blur 0.05 -quality 70% -colorspace GRAY";
                                if (ValidaPerfil)
                                    Depth += "-depth 8 -strip -interlace Plane -quality 70% -colorspace GRAY -resize 70%";
                            }
                            else
                            {
                                Depth += " -depth 1  -colorspace GRAY -resize 70%";
                            }
                        }
                    }

                    //espera en la cola determinado tiempo, mientras terminan los que se estan procesnado
                    lock (ReadOnly)
                    {
                        //entro
                        concurrent++;
                        //si actualmente el concurrente supero los maximos permitidos
                        if (concurrent > Settings.Default.MaximoConvertConcurrentes)
                        {


                            CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "esperando lugar para procesar: " + ", hora: " + System.DateTime.Now.ToLongDateString() + " " + System.DateTime.Now.ToLongTimeString());
                            Stopwatch timer = new Stopwatch();
                            timer.Start();
                            while (timer.Elapsed.TotalSeconds < Settings.Default.MaximoTiempoEsperaConvert)
                            {
                                if (concurrent <= Settings.Default.MaximoConvertConcurrentes)
                                {
                                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Entro a procesarse a las: " + ruta_archivo_temp + ", hora: " + System.DateTime.Now.ToLongDateString() + " " + System.DateTime.Now.ToLongTimeString());
                                    break;
                                }
                            }
                            timer.Stop();

                        }
                    }
                    //ejecuta el convert
                    /*
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    startInfo.FileName = Settings.Default.RutaConvertImageMagick;*/

                    long MuestraMemoria = 0;
                    long cont = 0;
                    //se toma el tiempo inicial antes del convert
                    DateTime TiempoConverts = System.DateTime.Now;

                    //log procesando tiff
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Procesando tiff en la ubicacion: " + Directorio + "\\" + Archivo + (CalidadImagen == 2 ? ".png" : ".jpeg"));
                    //String ArgumentosConvert = "\"" + ruta_archivo_temp + "\" " + Depth + " \"" + Directorio + "\\" + Archivo + (CalidadImagen == 2 ? ".png" : ".jpeg") + "\"";

                    Image image = Bitmap.FromFile(ruta_archivo_temp);
                    FrameDimension frameDimensions = new FrameDimension(image.FrameDimensionsList[0]);
                    int frameNum = image.GetFrameCount(frameDimensions);
                    //Argumentos convert en el log
                    /*
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "convert " + ArgumentosConvert);
                    startInfo.Arguments = ArgumentosConvert;
                    process.StartInfo = startInfo;
                    process.Start();
                    process.WaitForExit();
                     * */

                    for (int frame = 0; frame < frameNum; frame++)
                    {
                        // Selects one frame at a time and save as jpeg. 
                        image.SelectActiveFrame(frameDimensions, frame);
                        using (Bitmap bigimage = new Bitmap(image, 2000, 2500))
                        {
                            //crea la imagen grande
                            bigimage.Save(Directorio + "\\" + Archivo + "-" + Convert.ToString(frame) + (CalidadImagen == 2 ? ".png" : ".jpeg"), (CalidadImagen == 2 ? ImageFormat.Png : ImageFormat.Jpeg));
                            //crea la minuatura
                            if (RequiereMiniaturas)
                            {
                                using (Bitmap thumbnail = new Bitmap(image, 100, 100))
                                {
                                    thumbnail.Save(
                                        Directorio + "\\" + Archivo + "_Tmb-" + Convert.ToString(frame) +
                                        (CalidadImagen == 2 ? ".png" : ".jpeg"),
                                        (CalidadImagen == 2 ? ImageFormat.Png : ImageFormat.Jpeg));
                                }
                            }
                        }

                    }
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Finaliza Proceso tiff en la ubicacion: " + Directorio + "\\" + Archivo + "  paginas: " + Convert.ToString(frameNum));

                    //Obtener thumbs
                    //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    //startInfo.FileName = Settings.Default.RutaConvertImageMagick;
                    /*
                    if (RequiereMiniaturas)
                    {
                        ArgumentosConvert = "\"" + ruta_archivo_temp + "\" " + Depth + " -thumbnail 100x100 -compress JPEG \"" + Directorio + "\\" + Archivo + "_Tmb" + (CalidadImagen == 2 ? ".png" : ".jpeg") + "\"";
                        //Argumentos convert en el log
                        CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "convert " + ArgumentosConvert);
                        startInfo.Arguments = ArgumentosConvert;
                        process.StartInfo = startInfo;

                        process.Start();

                        process.WaitForExit();
                        //se XmlImplementation el metodo de net para crear miniatura
                   
                        CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "**Memoria ImageMagick Miniaturas: " + SizeSuffix(MuestraMemoria));
                    }
                    */



                    process.Close();

                    //reciclar variables
                    //GC.Collect();
                    //GC.WaitForPendingFinalizers();

                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "**Tiempo conversion " + Archivo + "(Inicio: " + TiempoConverts.ToLongTimeString() + "  Fin: " + System.DateTime.Now.ToLongTimeString() + "  ): " + (System.DateTime.Now - TiempoConverts).Seconds + "." + (System.DateTime.Now - TiempoConverts).Milliseconds + " Segundos");


                    return DirectorioBase;

                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                concurrent--;
            }
        }

        /// <summary>
        /// Procesa las imágenes con la libreria nativa de NET
        /// </summary>
        /// <returns>Ubicación de las imágenes divididas</returns>
        public String CrearPaginas(int CalidadImagen)
        {
            try
            {
                //reinicia contador
                if (concurrent < 0)
                    concurrent = 0;

                String Archivo = string.Empty;
                //informacion del archivo
                if (this.fileID != "")
                {
                    FileInfo file = new FileInfo(this.fileID);
                    Archivo = file.Name.Split('.')[0];
                    if (Archivo == "")
                    {
                        string[] Directories =
                        this.fileID.Split(Path.DirectorySeparatorChar);
                        Archivo = Directories[Directories.Count() - 2];
                    }
                }
                else
                {
                    FileInfo file = new FileInfo(this.m_FilePathName);
                    Archivo = file.Name.Split('.')[0];
                }
                //fileID = Path.GetTempPath() + Settings.Default.UbicacionImgCarpetaUnicaTEMP +Path.DirectorySeparatorChar + Archivo;
                String DirectorioAlta = fileID + Path.DirectorySeparatorChar + "AltaCalidad";
                String DirectorioBaja = fileID + Path.DirectorySeparatorChar + "BajaCalidad";

                try
                {
                    //si el directorio raiz no existe se crea 
                    if (!Directory.Exists(fileID))
                    {
                        Directory.CreateDirectory(fileID);
                        CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, " Se crea el directorio: " + fileID);
                    }

                    switch (this.calidad)
                    {
                        case 1:
                            if (Directory.Exists(DirectorioBaja))
                                Directory.Delete(DirectorioBaja,true);
                            
                            Directory.CreateDirectory(DirectorioBaja);
                            CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, " Se crea el directorio: " + DirectorioBaja);
                            break;
                        case 2:
                            if (Directory.Exists(DirectorioAlta))
                                Directory.Delete(DirectorioAlta, true);
                            
                            Directory.CreateDirectory(DirectorioAlta);
                            CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, " Se crea el directorio: " + DirectorioAlta);

                            
                            break;
                        case 3:

                            if (Directory.Exists(DirectorioBaja))
                                Directory.Delete(DirectorioBaja, true);
                            
                            Directory.CreateDirectory(DirectorioBaja);
                            CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, " Se crea el directorio: " + DirectorioBaja);

                            if (Directory.Exists(DirectorioAlta))
                                Directory.Delete(DirectorioAlta, true);
                            
                            Directory.CreateDirectory(DirectorioAlta);
                            CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, " Se crea el directorio: " + DirectorioAlta);

                            break;

                    }

                }
                catch (Exception ex)
                {

                    throw new Exception("No se pudo crear el directorio: " + fileID +
                                        " Verifique los permisos de escritura");
                }

                //Obtener grandes
                using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                {

                    /*
                    String Depth = "";
                    if (CalidadImagen == 2)
                    {
                        Depth += "-depth 1 -colorspace GRAY ";
                    }
                    else
                    {
                        using (Image img = Image.FromFile(this.m_FilePathName))
                        {
                            this.alto = img.Height;
                            this.ancho = img.Width;
                            this.paginas = img.GetFrameCount(FrameDimension.Page);
                            if (img.PixelFormat != PixelFormat.Format1bppIndexed)
                            {
                                //Depth += "-depth 8 -strip -interlace Plane -gaussian-blur 0.05 -quality 70% -colorspace GRAY";
                                if (ValidaPerfil)
                                    Depth += "-depth 8 -strip -interlace Plane -quality 70% -colorspace GRAY -resize 70%";
                            }
                            else
                            {
                                Depth += " -depth 1  -colorspace GRAY -resize 70%";
                            }

                        }
                    }
                    */

                    //espera en la cola determinado tiempo, mientras terminan los que se estan procesnado
                    lock (ReadOnly)
                    {
                        //entro
                        concurrent++;
                        //si actualmente el concurrente supero los maximos permitidos
                        if (concurrent > Settings.Default.MaximoConvertConcurrentes)
                        {
                            CreateTrace.WriteLog(CreateTrace.LogLevel.Debug,
                                "esperando lugar para procesar: " + ", hora: " + System.DateTime.Now.ToLongDateString() +
                                " " + System.DateTime.Now.ToLongTimeString());
                            Stopwatch timer = new Stopwatch();
                            timer.Start();
                            while (timer.Elapsed.TotalSeconds < Settings.Default.MaximoTiempoEsperaConvert)
                            {
                                if (concurrent <= Settings.Default.MaximoConvertConcurrentes)
                                {
                                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug,
                                        "Entro a procesarse a las: " + this.m_FilePathName + ", hora: " +
                                        System.DateTime.Now.ToLongDateString() + " " +
                                        System.DateTime.Now.ToLongTimeString());
                                    break;
                                }
                            }
                            timer.Stop();

                        }
                    }
                    //se toma el tiempo inicial antes del convert
                    DateTime TiempoConverts = System.DateTime.Now;
                    #region IMAGENMAGIC
                    /*
                    //escribimos en el log el apuntamiento de ImageMagick
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Ruta Image Magick: " + Settings.Default.RutaConvertImageMagick);

                    //verificamos que existea imageMagick
                    if (!File.Exists(Settings.Default.RutaConvertImageMagick))
                        throw new Exception("Por favor contacte al administrador: Image Magick no se encuentra instalado o se encuentra mal configurado");


                    //log procesando tiff
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug,
                        "Procesando tiff en la ubicacion: " + fileID + "\\" + Archivo +
                        (CalidadImagen == 2 ? ".png" : ".jpeg"));

                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    startInfo.FileName = Settings.Default.RutaConvertImageMagick;


                    //crea la imagen grande calidad alta
                    String ArgumentosConvert = "\"" + this.m_FilePathName + "\" " + Depth + " \"" + DirectorioAlta + "\\" + Archivo + ".jpeg" + "\"";
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "convert " + ArgumentosConvert);
                    startInfo.Arguments = ArgumentosConvert;
                    process.StartInfo = startInfo;
                    process.Start();
                    process.WaitForExit();

                    //se crea la imagen grande de baja calidad
                    ArgumentosConvert = "\"" + this.m_FilePathName + "\" " + Depth + " \"" + DirectorioBaja + "\\" + Archivo + ".png" + "\"";
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "convert " + ArgumentosConvert);
                    startInfo.Arguments = ArgumentosConvert;
                    process.StartInfo = startInfo;
                    process.Start();
                    process.WaitForExit();

                    //se crea la miniatura de alta calidad
                    ArgumentosConvert = "\"" + this.m_FilePathName + "\" " + Depth + " -thumbnail 100x100 -compress JPEG \"" + DirectorioAlta + "\\" + Archivo + "_Tmb.jpeg" + "\"";
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "convert " + ArgumentosConvert);
                    startInfo.Arguments = ArgumentosConvert;
                    process.StartInfo = startInfo;
                    process.Start();
                    process.WaitForExit();

                    //Se crea la miniatura de baja
                    ArgumentosConvert = "\"" + this.m_FilePathName + "\" " + Depth + " -thumbnail 100x100 -compress JPEG \"" + DirectorioBaja + "\\" + Archivo + "_Tmb.png" + "\"";
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "convert " + ArgumentosConvert);
                    startInfo.Arguments = ArgumentosConvert;
                    process.StartInfo = startInfo;
                    process.Start();
                    process.WaitForExit();
                    */
                    #endregion


                    #region IMAGEN MAGICK DLL
                    MagickNET.SetTempDirectory(Path.GetTempPath());
                    ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
                    ImageCodecInfo ici = null;

                    foreach (ImageCodecInfo codec in codecs)
                    {
                        if (codec.MimeType == "image/jpeg")
                        {
                            ici = codec;
                            break;
                        }
                    }
                    EncoderParameters ep = new EncoderParameters();
                    ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)15);
                    Boolean pdf = m_FilePathName.ToLower().IndexOf(".pdf") >= 0;
                    int indice = 0;
                    FileInfo FI = new FileInfo(this.m_FilePathName);
                    using (MagickImageCollection imageCol = ((pdf) ? new MagickImageCollection() : new MagickImageCollection(FI)))
                    {

                        if (pdf)
                        {

                            MagickReadSettings settings = new MagickReadSettings();
                            settings.Density = new Density(200, 200);                            
                            imageCol.Read(this.m_FilePathName, settings);                            
                        }
                        CreateTrace.WriteLog(CreateTrace.LogLevel.Debug,
                        "Tiempo Carga Imagen " + Archivo + "(Inicio: " + TiempoConverts.ToLongTimeString() + "  Fin: " +
                        System.DateTime.Now.ToLongTimeString() + "  ): " +
                        (System.DateTime.Now - TiempoConverts).Seconds + "." +
                        (System.DateTime.Now - TiempoConverts).Milliseconds + " Segundos");
                        int imagenes = imageCol.Count;
                        foreach (MagickImage image in imageCol)
                        {

                            //Depth += "-depth 1 -colorspace GRAY ";
                            this.alto = image.Height;
                            this.ancho = image.Width;
                            //se definen las propiedades de la imagen
                            image.CompressionMethod = CompressionMethod.LZW;
                            if (pdf)
                            {
                                image.Alpha(AlphaOption.Remove);
                                image.Resize(2000, 2500);
                                image.Quality = 80;
                                image.Scale(new Percentage(80));
                            }
                            else
                            {
                                image.Quality = 70;
                                image.Scale(new Percentage(70));
                                image.ColorSpace = ColorSpace.Gray;
                            }
                            //image.Interlace = Interlace.Plane;
                            Density dn = new Density(200);
                            image.Density = dn;
                            //se crean las imagenes
                            image.Depth = 2;
                            switch (this.calidad)
                            {
                                case 1:
                                    image.Write(DirectorioBaja + "\\" + Archivo + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".png");
                                    break;
                                case 2:
                                    //crea la imagen grande calidad alta
                                    image.Write(DirectorioAlta + "\\" + Archivo + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".jpeg");
                                    break;
                                case 3:
                                    image.Write(DirectorioBaja + "\\" + Archivo + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".png");
                                    image.Write(DirectorioAlta + "\\" + Archivo + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".jpeg");
                                    break;
                            }
                                                        
                            Image imageT = new Bitmap(DirectorioAlta + "\\" + Archivo + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".jpeg");
                            Image thumbnail = imageT.GetThumbnailImage(100, 100, null, new IntPtr());
                            //se crean las miniaturas
                            switch (this.calidad)
                            {
                                case 1:
                                    thumbnail.Save(DirectorioBaja + "\\" + Archivo + "_Tmb" + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".png", ici, ep);
                                    break;
                                case 2:
                                    //se crea la miniatura de alta calidad
                                    thumbnail.Save(DirectorioAlta + "\\" + Archivo + "_Tmb" + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".jpeg", ici, ep);
                                    break;
                                case 3:
                                    //se crea la miniatura de alta calidad y Baja calidad
                                    thumbnail.Save(DirectorioBaja + "\\" + Archivo + "_Tmb" + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".png", ici, ep);
                                    thumbnail.Save(DirectorioAlta + "\\" + Archivo + "_Tmb" + (imagenes == 1 ? "" : "-" + Convert.ToString(indice)) + ".jpeg", ici, ep);
                                    break;
                            }
                            indice = indice + 1;
                            image.Dispose();
                            thumbnail.Dispose();
                            imageT.Dispose();
                        }
                        imageCol.Dispose();
                        this.paginas = indice;
                    }
                    #endregion
                    #region IMAGEN NET
                    /*

                    
                    Image image = Bitmap.FromFile(this.m_FilePathName);
                    FrameDimension frameDimensions = new FrameDimension(image.FrameDimensionsList[0]);
                    int frameNum = image.GetFrameCount(frameDimensions);
                    for (int frame = 0; frame < frameNum; frame++)
                    {
                        // Selects one frame at a time and save as jpeg. 
                        try
                        {
                            image.SelectActiveFrame(frameDimensions, frame);
                            using (Bitmap bigimage = new Bitmap(image, 2000, 2500))
                            {

                               //crea la imagen grande calidad alta
                                bigimage.Save(DirectorioAlta + "\\" + Archivo + "-" + Convert.ToString(frame) + ".jpeg",
                                    ImageFormat.Jpeg);
                                //se crea la imagen grande calidad baja
                         
                                if (CalidadImagen == 2)
                                    bigimage.Save(
                                        DirectorioBaja + "\\" + Archivo + "-" + Convert.ToString(frame) + ".png",
                                        ImageFormat.Png);
                                bigimage.Dispose();
                                GC.SuppressFinalize(bigimage);
                                //crea la minuatura
                                if (RequiereMiniaturas)
                                {
                                    using (Bitmap thumbnail = new Bitmap(image, 100, 100))
                                    {
                                        //se crea la miniatura de alta calidad
                                        thumbnail.Save(
                                            DirectorioAlta + "\\" + Archivo + "_Tmb-" + Convert.ToString(frame) +
                                            ".jpeg",
                                            ImageFormat.Jpeg);
                                        //se crea la miniatura de baja
                                        if (CalidadImagen == 2)
                                            thumbnail.Save(
                                                DirectorioBaja + "\\" + Archivo + "_Tmb-" + Convert.ToString(frame) +
                                                ".png",
                                                ImageFormat.Png);
                                        thumbnail.Dispose();
                                        GC.SuppressFinalize(thumbnail);
                                    }
                                }
                                 
                            }
                        }
                        catch
                            (Exception exc)
                        {
                            CreateTrace.WriteLog(CreateTrace.LogLevel.Error,
                                " Error convirtiendo : " + fileID + "     " + exc.Message);
                            throw exc;
                        }

                    }
                    */
                    #endregion
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug,
                        "Finaliza Proceso tiff en la ubicacion: " + fileID + "     " + Archivo + "  paginas: " +
                        Convert.ToString(this.paginas));

                    process.Close();

                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug,
                        "**Tiempo conversion " + Archivo + "(Inicio: " + TiempoConverts.ToLongTimeString() + "  Fin: " +
                        System.DateTime.Now.ToLongTimeString() + "  ): " +
                        (System.DateTime.Now - TiempoConverts).Seconds + "." +
                        (System.DateTime.Now - TiempoConverts).Milliseconds + " Segundos");




                }
            }
            catch (Exception exc)
            {
                CreateTrace.WriteLog(CreateTrace.LogLevel.Error, " Error en el procesador Imágenes: " + fileID + "     " + exc.Message);
                throw exc;
            }
            finally
            {
                concurrent--;
            }
            return fileID;
        }


        public string SizeSuffix(Int64 value)
        {

            return (value / 1024f) / 1024f + "MB";
        }
        /// <summary>
        /// Returns an Image of a TIF page, resized
        /// </summary>
        /// <param name="PageNum"></param>
        /// <returns></returns>
        public TIFF_Image GetTiffImageThumb(int PageNum, int ImgWidth, int ImgHeight)
        {
            //if ((PageNum < 1) | (PageNum > this.PageCount))
            //{
            //    throw new InvalidOperationException("Page to be retrieved is outside the bounds of the total TIF file pages.  Please choose a page number that exists.");
            //}

            //String ruta_archivo_temp = this.m_FilePathName;

            //FileInfo file = new FileInfo(ruta_archivo_temp);
            //String Archivo = file.Name.Split('.')[0];
            //String Directorio = Path.GetTempPath() + Settings.Default.UbicacionImgCarpetaUnicaTEMP + "\\" + Archivo;

            //if (EsBajaCalidad())
            //{
            //    Directorio += "_BN_";
            //}

            //String DirectorioConArchivo = Directorio + "\\" + Archivo;


            //if (!Directory.Exists(Directorio))
            //{
            //    Directory.CreateDirectory(Directorio);
            //}

            //if (!File.Exists(DirectorioConArchivo + "_Tmb-" + (PageNum - 1) + (EsBajaCalidad() ? ".png" : ".jpeg")))
            //    CrearArchivosPaginas();

            //if ((new DirectoryInfo(Directorio)).GetFiles().Length > 1)
            //{
            //    String rutaArchivo = DirectorioConArchivo + "_Tmb-" + (PageNum - 1) + (EsBajaCalidad() ? ".png" : ".jpeg");
            //    return new TIFF_Image() { Imagen = System.Drawing.Image.FromFile(rutaArchivo), RutaImagen = rutaArchivo, Formato = (EsBajaCalidad() ? ImageFormat.Png : ImageFormat.Jpeg) };
            //}
            //else
            //{
            //    String rutaArchivo = DirectorioConArchivo + "_Tmb" + (EsBajaCalidad() ? ".png" : ".jpeg");
            //    return new TIFF_Image() { Imagen = System.Drawing.Image.FromFile(rutaArchivo), RutaImagen = rutaArchivo, Formato = (EsBajaCalidad() ? ImageFormat.Png : ImageFormat.Jpeg) };
            //}

            throw new NotImplementedException();
        }


        /// <summary>
        /// Verifica el perfil, esto para saber en que Calidad de Imagen se renderizará
        /// </summary>
        /// <returns></returns>
        public bool EsBajaCalidad()
        {
            try
            {

                try
                {
                    if (ValidaPerfil)
                    {
                        switch (calidad)
                        {
                            case 2:
                                return true;
                            default:
                                return false;
                        }
                    }
                    else
                        return false;

                }
                catch (Exception exc)
                {
                    //si ocurre un error se guardad el log, y se retorna que es de Alta Calidad
                    CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Hubo un error al verificar los permisos de calidad de imagen: " + exc.Message);
                }

                return false;
            }
            catch (Exception exc)
            {
                //si ocurre un error se guardad el log, y se retorna que es de Alta Calidad
                CreateTrace.WriteLog(CreateTrace.LogLevel.Debug, "Hubo un error al verificar los permisos de calidad de imagen: " + exc.Message);
            }

            return false;
        }


        private static Image ResizeImage(double newSize, Image originalImage)
        {
            double newHeight = originalImage.Height;
            if (newSize <= originalImage.Width)
            {

                double porcentaje = newSize / originalImage.Width;

                newSize = originalImage.Width * porcentaje;
                newHeight = originalImage.Height * porcentaje;

            }


            return originalImage.GetThumbnailImage(Convert.ToInt32(newSize), Convert.ToInt32(newHeight), null, IntPtr.Zero);
        }

        /// <summary>
        /// Returns an images beased on pages specified
        /// </summary>
        /// <param name="StartPageNum"></param>
        /// <param name="EndPageNum"></param>
        /// <returns></returns>
        public TIFPageCollection GetTiffImages(int StartPageNum, int EndPageNum)
        {
            TIFPageCollection Pgs = new TIFPageCollection();
            if (((StartPageNum < 1) | (EndPageNum > this.m_PageCount)) | (EndPageNum > StartPageNum))
            {
                throw new InvalidOperationException("Page being retrieved is outside the bounds of the total TIF file pages.  Please choose a page number that exists.");
            }
            try
            {
                int TotPgs = EndPageNum - StartPageNum;
                for (int i = 0; i <= TotPgs; i++)
                {
                    Pgs.Add(this.GetTiffImage(StartPageNum + i).Imagen);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            /*finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }*/
            return Pgs;
        }

        /// <summary>
        /// Returns an images based on pages specified, resized
        /// </summary>
        /// <param name="StartPageNum"></param>
        /// <param name="EndPageNum"></param>
        /// <returns></returns>
        public System.Drawing.Image[] GetTiffImageThumbs(int StartPageNum, int EndPageNum, int ImgWidth, int ImgHeight)
        {
            TIFPageCollection Pgs = new TIFPageCollection();
            if (((StartPageNum < 1) || (EndPageNum > this.m_PageCount)) || (EndPageNum < StartPageNum))
            {
                throw new InvalidOperationException("Page being retrieved is outside the bounds of the total TIF file pages.  Please choose a page number that exists.");
            }
            Image[] returnImage = new Image[(EndPageNum - StartPageNum) + 1];
            try
            {
                int TotPgs = EndPageNum - StartPageNum;
                for (int i = 0; i <= TotPgs; i++)
                {
                    returnImage[i] = this.GetTiffImageThumb(StartPageNum + i, ImgWidth, ImgHeight).Imagen;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            /*finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }*/
            return returnImage;
        }

        /// <summary>
        /// Returns an images based on pages specified, resized
        /// </summary>
        /// <param name="StartPageNum"></param>
        /// <param name="EndPageNum"></param>
        /// <returns></returns>
        public TIFPageCollection GetTiffImageThumbsCollection(int StartPageNum, int EndPageNum, int ImgWidth, int ImgHeight)
        {
            TIFPageCollection Pgs = new TIFPageCollection();
            if (((StartPageNum < 1) || (EndPageNum > this.m_PageCount)) || (EndPageNum < StartPageNum))
            {
                throw new InvalidOperationException("Page being retrieved is outside the bounds of the total TIF file pages.  Please choose a page number that exists.");
            }
            try
            {
                int TotPgs = EndPageNum - StartPageNum;
                for (int i = 0; i <= TotPgs; i++)
                {
                    Pgs.Add(this.GetTiffImageThumb(StartPageNum + i, ImgWidth, ImgHeight).Imagen);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
            }
            return Pgs;
        }

        /// <summary>
        /// Returns a Image of a specific page
        /// </summary>
        /// <param name="PageNum"></param>
        /// <returns></returns>
        public Image this[int PageNum]
        {
            get
            {
                Image TiffPage;
                try
                {
                    this.m_Img = this.GetTiffImage(PageNum).Imagen;
                    TiffPage = this.m_Img;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return TiffPage;
            }
        }

        #endregion


        internal MemoryStream getThumbnailImageMagick(MemoryStream Stream, double newSize)
        {

            Image imagen = ResizeImage(newSize, Image.FromStream(Stream));
            MemoryStream ms = new MemoryStream();
            imagen.Save(ms, ImageFormat.Png);
            return ms;

        }

        public int calidad { get; set; }
        public int ancho { get; set; }
        public int alto { get; set; }
        public int paginas { get; set; }

    }

    /// <summary>
    /// Collection of objects
    /// </summary>
    [Serializable]
    public class TIFPageCollection : System.Collections.CollectionBase
    {
        private bool m_Disposed { get; set; }

        #region CONSTURCTORS

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TIFPageCollection() { }

        /// <summary>
        /// Disposes of the object by clearing the collection
        /// </summary>
        public void Dispose()
        {   //Make sure each image is disposed of properly
            foreach (System.Drawing.Image Img in this)
            {
                Img.Dispose();
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
            }
            this.m_Disposed = true;
            this.Clear();
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Adds an item to the collection
        /// </summary>
        /// <param name="Obj"></param>
        public void Add(System.Drawing.Image Obj)
        {
            this.List.Add(Obj);
        }

        /// <summary>
        /// Indicates if the object exists in the collection
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public bool Contains(System.Drawing.Image Obj)
        {
            return this.List.Contains(Obj);
        }

        /// <summary>
        /// Returns the indexOf the object
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public int IndexOf(System.Drawing.Image Obj)
        {
            return this.List.IndexOf(Obj);
        }

        /// <summary>
        /// Inserts an object into the collection at the specifed index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="Obj"></param>
        public void Insert(int index, System.Drawing.Image Obj)
        {
            this.List.Insert(index, Obj);
        }

        /// <summary>
        /// Removes the object from the collection
        /// </summary>
        /// <param name="Obj"></param>
        public void Remove(System.Drawing.Image Obj)
        {
            this.List.Remove(Obj);
        }

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Returns a reference to the object at specified index
        /// </summary>
        /// <param name="index">Index of object</param>
        /// <returns></returns>
        public System.Drawing.Image this[int index]
        {
            get
            {
                return (System.Drawing.Image)this.List[index];
            }
            set
            {
                this.List[index] = value;
            }
        }

        #endregion

    }

}
